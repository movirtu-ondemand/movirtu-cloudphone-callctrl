/*
 *  Copyright 2004-2006 Stefan Reuter
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package org.asteriskjava.manager.action;
import org.asteriskjava.manager.event.MessageSendEvent;
import org.asteriskjava.manager.event.ResponseEvent;

import java.util.*;


/**
 * 
 * @author srt
 * @version $Id$
 * @see org.asteriskjava.manager.event.MessageSendResponseEvent
 */
public class MessageSendAction extends AbstractManagerAction implements EventGeneratingAction
{
    /**
     * Serializable version identifier
     */
    static final long serialVersionUID = 9194557741043334704L;

    
    private Map<String, String> variables;
	private String to;
    private String from;
    private String body;
  

    /**
     * Returns the name of this action, i.e. "Originate".
     *
     * @return the name of this action.
     */
    @Override
    public String getAction()
    {
        return "MessageSend";
    }

    
   

    /**
     * Sets an variable on the originated call.
     *
     * @param name  the name of the variable to set.
     * @param value the value of the variable to set.
     * @since 0.3
     */
    public void setVariable(String name, String value)
    {
        if (variables == null)
        {
            variables = new LinkedHashMap<String, String>();
        }

        variables.put(name, value);
    }

    /**
     * Returns the variables to set on the originated call.
     *
     * @return a Map containing the variable names as key and their
     *         values as value.
     * @since 0.2
     */
    public Map<String, String> getVariables()
    {
        return variables;
    }

    /**
     * Sets the variables to set on the originated call.
     *
     * @param variables a Map containing the variable names as key and their
     *                  values as value.
     * @since 0.2
     */
    public void setVariables(Map<String, String> variables)
    {
        this.variables = variables;
    }

    

    public Class<? extends ResponseEvent> getActionCompleteEventClass()
    {
        return MessageSendEvent.class;
    }
    public String getTo() {
    	return to;
	}
	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
