package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface DialplanCEP {

// void setContext String
/**********************************************************
 * Context in the dialplan
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setContext(String val );



// long getPriority
/**********************************************************
 * Priority in the dialplan
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public long getPriority();



// void setPriority long
/**********************************************************
 * Priority in the dialplan
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setPriority(long val );



// String getExten
/**********************************************************
 * Extension in the dialplan
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getExten();



// String getContext
/**********************************************************
 * Context in the dialplan
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getContext();



// void setExten String
/**********************************************************
 * Extension in the dialplan
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setExten(String val );


}
;
