package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface PlaybackStarted {

// void setPlayback Playback
/**********************************************************
 * Playback control object
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setPlayback(Playback val );



// Playback getPlayback
/**********************************************************
 * Playback control object
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public Playback getPlayback();


}
;
