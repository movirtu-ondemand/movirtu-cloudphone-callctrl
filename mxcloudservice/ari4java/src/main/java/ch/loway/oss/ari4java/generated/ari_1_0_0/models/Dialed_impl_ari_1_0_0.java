package ch.loway.oss.ari4java.generated.ari_1_0_0.models;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import ch.loway.oss.ari4java.generated.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import java.util.List;

/**********************************************************
 * Dialed channel information.
 * 
 * Defined in file: channels.json
 *********************************************************/

public class Dialed_impl_ari_1_0_0 implements Dialed, java.io.Serializable {
private static final long serialVersionUID = 1L;
/** No missing signatures from interface */
}

