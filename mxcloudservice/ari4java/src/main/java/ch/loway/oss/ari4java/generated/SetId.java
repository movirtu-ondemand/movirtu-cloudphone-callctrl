package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface SetId {

// void setUser String
/**********************************************************
 * Effective user id.
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setUser(String val );



// void setGroup String
/**********************************************************
 * Effective group id.
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setGroup(String val );



// String getUser
/**********************************************************
 * Effective user id.
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getUser();



// String getGroup
/**********************************************************
 * Effective group id.
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getGroup();


}
;
