package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface StasisStart {

// void setArgs List<? extends String>
/**********************************************************
 * Arguments to the application
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setArgs(List<? extends String> val );



// List<? extends String> getArgs
/**********************************************************
 * Arguments to the application
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public List<? extends String> getArgs();



// Channel getChannel
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public Channel getChannel();



// void setChannel Channel
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setChannel(Channel val );


}
;
