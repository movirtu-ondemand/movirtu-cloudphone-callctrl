package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface Variable {

// void setValue String
/**********************************************************
 * The value of the variable requested
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setValue(String val );



// String getValue
/**********************************************************
 * The value of the variable requested
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getValue();


}
;
