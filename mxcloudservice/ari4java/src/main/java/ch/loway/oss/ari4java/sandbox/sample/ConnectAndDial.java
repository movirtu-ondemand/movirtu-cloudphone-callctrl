package ch.loway.oss.ari4java.sandbox.sample;

import ch.loway.oss.ari4java.ARI;
import ch.loway.oss.ari4java.tools.ARIException;
import ch.loway.oss.ari4java.AriVersion;
import ch.loway.oss.ari4java.generated.ActionEvents;
import ch.loway.oss.ari4java.generated.Application;
import ch.loway.oss.ari4java.generated.AsteriskInfo;
import ch.loway.oss.ari4java.generated.Bridge;
import ch.loway.oss.ari4java.generated.ChannelDestroyed;
import ch.loway.oss.ari4java.generated.ChannelHangupRequest;
import ch.loway.oss.ari4java.generated.ChannelStateChange;
import ch.loway.oss.ari4java.generated.ChannelVarset;
import ch.loway.oss.ari4java.generated.Message;
import ch.loway.oss.ari4java.generated.StasisStart;
import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.MessageQueue;
import ch.loway.oss.ari4java.tools.RestException;

import java.util.List;

/**
 * This class opens up an ARI application that creates a bridge with MOH.
 * When a call enters the application, a message is printed and the call
 * is connected to the bridge.
 *
 * @author lenz
 */
public class ConnectAndDial {

    ARI ari = null;
    Bridge b = null;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConnectAndDial me = new ConnectAndDial();
        me.start();
    }

    /**
     * This is the app...
     * 
     */
    public void start() {
		while(true) {
		        try {
		
		            connect();
		            //createBridge();
		            processEvents();
		            //processEvents_old();
		
		            //removeBridge();
		
		        } catch (ARIException e) {
		            e.printStackTrace();
		        } finally {
		            if (ari != null) {
		                try {
		                    ARI.sleep(500);
		                    ari.cleanup();
		                } catch (Throwable t) {
		                }
		            }
		        }
		}
    }

    public void connect() throws ARIException {

        ari = ARI.build("http://54.72.39.131:8088/", "callctrl", "hey1", "peekaboo", AriVersion.ARI_1_0_0);

        // lets raise an exeption if the connection is not valid
        AsteriskInfo ai = ari.asterisk().getInfo("");
        System.out.println("Hey! We're connected! Asterisk Version: " + ai.getSystem().getVersion());

    }

    public void createBridge() throws ARIException {

        // create a bridge and start playing MOH on it
        // UGLY: we should have a constant for the allowed bridge types
        b = ari.bridges().create("", "myBridge");
        System.out.println("Bridge ID:" + b.getId() + " Name:" + b.getName() + " Tech:" + b.getTechnology() + " Creator:" + b.getCreator());


        // start MOH on the bridge
        ari.bridges().startMoh(b.getId(), "");

        // check which bridges are available
        // UGLY: why not a List<Bridges>? the cliens should not care....
        List<? extends Bridge> bridges = ari.bridges().list();

        for (Bridge bb : bridges) {
            printBridge(bb);
        }

    }

    /**
     * This code is ugly.
     * we should have only one websocket, per app, not linked to an action....
     * 
     * @throws ARIException
     */
    public void processEvents_old() throws ARIException {

        ActionEvents ae = ari.events();
        ae.eventWebsocket("callctrl", new AriCallback<Message>() {

            @Override
            public void onSuccess(Message result) {
                // Let's do something with the event

                System.out.println(result);

                if (result instanceof StasisStart) {
                    StasisStart event = (StasisStart) result;
                    System.out.println("Channel found: " + event.getChannel().getId() + " State:" + event.getChannel().getState());
                    try {
						ari.channels().continueInDialplan(event.getChannel().getId(), "dial-context", "3105", 1);
					} catch (RestException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                   
                    /*
                    try {
                        ari.bridges().addChannel(b.getId(), event.getChannel().getId(), "");
                    } catch (ARIException e) {
                        // UGLY!!
                        System.out.println("Error: " + e.getMessage());
                    }*/
                }



            }

            @Override
            public void onFailure(RestException e) {
                e.printStackTrace();
            }
        });

        try {
        	while(true)
            Thread.sleep(60000); // Wait 5 seconds for events
        } catch (InterruptedException e) {
        }

        // UGLIEST - if you do a closeaction, everything dies and no more actions
        // are sent
        ari.closeAction(ae); // Now close the websocket

    }

    /**
     * The new style of event processing...
     *
     * @throws ARIException
     */


    public void processEvents() throws ARIException {

        System.out.println( "Starting events... " );
        
        MessageQueue mq = ari.getWebsocketQueue();
        /*
        while(true) {
	        try {
				mq.wait(2000);
				System.out.println("Waiting....");
				Message m = mq.dequeueMax( 100, 20 );
	            if (m != null) {

	                //long now = System.currentTimeMillis() - start;
	                //System.out.println(now + ": " + m);

	                if (m instanceof StasisStart) {
	                    StasisStart event = (StasisStart) m;
	                    System.out.println("Channel found: " + event.getChannel().getId() + " State:" + event.getChannel().getState());

	                    ///ari.bridges().addChannel(b.getId(), event.getChannel().getId(), "");
	                }
	            } 
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        */
        long start = System.currentTimeMillis();
        while(true) {
        //while ((System.currentTimeMillis() - start) < 2 * 1000L) {

            Message m = mq.dequeueMax( 100, 20 );
            if (m != null) {

                long now = System.currentTimeMillis() - start;
                System.out.println(now + ": " + m);

                if (m instanceof StasisStart) {
                	onStasisStart((StasisStart) m);
                    //ari.bridges().addChannel(b.getId(), event.getChannel().getId(), "");
                } else if (m instanceof ChannelVarset) {
                	onChannelVariableChange((ChannelVarset) m);
                } else if (m instanceof ChannelStateChange) {
                	onChannelStateChange((ChannelStateChange) m);
                } else if (m instanceof ChannelHangupRequest) {
                	onChannelHangupRequest((ChannelHangupRequest) m);
                } else if (m instanceof ChannelDestroyed) {
                	onChannelDestroyed((ChannelDestroyed) m);
                }
            } 
        }

        //System.out.println( "No more events... " );
    }

    public void onChannelHangupRequest(ChannelHangupRequest m) {
    	System.out.println("Channel id :" +m.getChannel().getId()+" hangup request, cause = "+m.getCause());
    }
    
    public void onChannelDestroyed(ChannelDestroyed m) {
    	System.out.println("Channel id : "+m.getChannel().getId()+" destroyed, cause = "+m.getCause_txt());
    }
    
    public void onChannelStateChange(ChannelStateChange m) {
    	System.out.println("Channel id : "+m.getChannel().getId()+" Channel State = "+m.getChannel().getState());
    	if(m.getChannel().getState().equalsIgnoreCase("UP")) {
    		//channel is up.. let's destroy it
    		ari.channels().hangup(m.getChannel().getId(), "normal", new AriCallback<Void>() {

				@Override
				public void onSuccess(Void result) {
					// TODO Auto-generated method stub
					System.out.println("hangup request successful");
				}

				@Override
				public void onFailure(RestException e) {
					// TODO Auto-generated method stub
					System.out.println("hangup failed");
					e.printStackTrace();
				}
    			
    		});
    	}
    }
    
    public void onChannelVariableChange(ChannelVarset event) {
    	System.out.println("Channel Variable Change : "+"Channel id : "+event.getChannel().getId()+" "+event.getVariable()+ " = "+event.getValue());
    	
    }
    public void onStasisStart(StasisStart event) {
        System.out.println("Channel found: " + event.getChannel().getId() + " State:" + event.getChannel().getState());
        
        ari.applications().subscribe("callctrl", "channel:"+event.getChannel().getId() , new AriCallback<Application> () {
        	@Override
		    public void onSuccess(Application result) {
		        // Let's do something with the returned value
		        System.out.println("Application : "+ result + " subscribed successfully");
		    }

		    @Override
		    public void onFailure(RestException e) {
		        System.out.println( "Failure in subscribing to application ");
		        e.printStackTrace();
		    }
        });
        
        ari.channels().setChannelVar(event.getChannel().getId(), "CALLERID(num)", "9910199554", new AriCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onFailure(RestException e) {
				// TODO Auto-generated method stub
				e.printStackTrace();
			}
        	
        });
        
        ari.channels().continueInDialplan(event.getChannel().getId(), "dial-trunk", "53105", 1, new AriCallback<Void>() {
			@Override
		    public void onSuccess(Void result) {
		        // Let's do something with the returned value
		        System.out.println("continueInDialPlan successful");
		    }

		    @Override
		    public void onFailure(RestException e) {
		        System.out.println( "continueInDialPlan failed ");
		        e.printStackTrace();
		    }
		});

    }
    public void removeBridge() throws ARIException {

        System.out.println("Removing....");

        ari.bridges().destroy(b.getId(), new AriCallback<Void>() {

            @Override
            public void onSuccess(Void result) {
                // Let's do something with the returned value
                System.out.println("Bridge destroyed");
            }

            @Override
            public void onFailure(RestException e) {
                System.out.println( "Failure removeBridge() ");
                e.printStackTrace();
            }
        });
    }

    /**
     * Dumps a bridge to string.
     * Should we have a default toString that makes more sense?
     * 
     * @param b
     */
    private void printBridge(Bridge b) {
        System.out.println("Bridge ID:" + b.getId()
                + " Name:" + b.getName()
                + " Tech:" + b.getTechnology()
                + " Creator:" + b.getCreator()
                + " Class: " + b.getBridge_class()
                + " Type: " + b.getBridge_type()
                + " Chans: " + b.getChannels().size());
        for (String s : b.getChannels()) {
            System.out.println(" - ID: " + s);
        }

    }
}
