package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface LiveRecording {

// String getFormat
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getFormat();



// void setState String
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setState(String val );



// void setName String
/**********************************************************
 * Base name for the recording
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setName(String val );



// String getCause
/**********************************************************
 * Cause for recording failure if failed
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getCause();



// String getName
/**********************************************************
 * Base name for the recording
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getName();



// void setFormat String
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setFormat(String val );



// void setCause String
/**********************************************************
 * Cause for recording failure if failed
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setCause(String val );



// String getState
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getState();


}
;
