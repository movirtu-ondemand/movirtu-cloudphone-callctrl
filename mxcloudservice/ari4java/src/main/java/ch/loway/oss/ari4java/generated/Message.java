package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface Message {

// void setType String
/**********************************************************
 * Indicates the type of this message.
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setType(String val );



// String getType
/**********************************************************
 * Indicates the type of this message.
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getType();


}
;
