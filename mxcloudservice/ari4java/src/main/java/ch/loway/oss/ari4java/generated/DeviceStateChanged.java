package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface DeviceStateChanged {

// void setDevice_state DeviceState
/**********************************************************
 * Device state object
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setDevice_state(DeviceState val );



// DeviceState getDevice_state
/**********************************************************
 * Device state object
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public DeviceState getDevice_state();


}
;
