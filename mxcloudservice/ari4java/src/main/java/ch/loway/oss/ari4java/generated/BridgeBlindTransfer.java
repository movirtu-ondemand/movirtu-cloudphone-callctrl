package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface BridgeBlindTransfer {

// void setIs_external boolean
/**********************************************************
 * Whether the transfer was externally initiated or not
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public void setIs_external(boolean val );



// void setContext String
/**********************************************************
 * The context transferred to
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public void setContext(String val );



// String getResult
/**********************************************************
 * The result of the transfer attempt
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public String getResult();



// void setBridge Bridge
/**********************************************************
 * The bridge being transferred
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public void setBridge(Bridge val );



// String getExten
/**********************************************************
 * The extension transferred to
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public String getExten();



// boolean getIs_external
/**********************************************************
 * Whether the transfer was externally initiated or not
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public boolean getIs_external();



// String getContext
/**********************************************************
 * The context transferred to
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public String getContext();



// Channel getChannel
/**********************************************************
 * The channel performing the blind transfer
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public Channel getChannel();



// void setResult String
/**********************************************************
 * The result of the transfer attempt
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public void setResult(String val );



// Bridge getBridge
/**********************************************************
 * The bridge being transferred
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public Bridge getBridge();



// void setExten String
/**********************************************************
 * The extension transferred to
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public void setExten(String val );



// void setChannel Channel
/**********************************************************
 * The channel performing the blind transfer
 * 
 * @since: ari_1_2_0
 *********************************************************/
 public void setChannel(Channel val );


}
;
