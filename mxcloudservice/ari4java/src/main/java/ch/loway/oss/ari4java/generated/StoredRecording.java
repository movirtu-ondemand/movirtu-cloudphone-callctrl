package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface StoredRecording {

// String getFormat
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getFormat();



// void setName String
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setName(String val );



// String getName
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getName();



// void setFormat String
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setFormat(String val );


}
;
