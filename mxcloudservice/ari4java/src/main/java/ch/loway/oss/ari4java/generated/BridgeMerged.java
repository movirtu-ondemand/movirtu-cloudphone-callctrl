package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface BridgeMerged {

// void setBridge_from Bridge
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setBridge_from(Bridge val );



// void setBridge Bridge
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setBridge(Bridge val );



// Bridge getBridge_from
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public Bridge getBridge_from();



// Bridge getBridge
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public Bridge getBridge();


}
;
