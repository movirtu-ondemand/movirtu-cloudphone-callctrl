package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface FormatLangPair {

// String getFormat
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getFormat();



// String getLanguage
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public String getLanguage();



// void setLanguage String
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setLanguage(String val );



// void setFormat String
/**********************************************************
 * 
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setFormat(String val );


}
;
