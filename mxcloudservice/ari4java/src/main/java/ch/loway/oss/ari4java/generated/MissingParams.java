package ch.loway.oss.ari4java.generated;

// ----------------------------------------------------
//      THIS CLASS WAS GENERATED AUTOMATICALLY         
//               PLEASE DO NOT EDIT                    
// ----------------------------------------------------

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import ch.loway.oss.ari4java.tools.RestException;
import ch.loway.oss.ari4java.tools.AriCallback;

public interface MissingParams {

// List<? extends String> getParams
/**********************************************************
 * A list of the missing parameters
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public List<? extends String> getParams();



// void setParams List<? extends String>
/**********************************************************
 * A list of the missing parameters
 * 
 * @since: ari_0_0_1
 *********************************************************/
 public void setParams(List<? extends String> val );


}
;
