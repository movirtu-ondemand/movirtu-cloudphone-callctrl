package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public class FetchCalledPartyMsisdnAction extends CallBaseAction {

	public boolean perform(Session session) throws Exception, RestException {
		System.out.println("Inside "+getName());
		String calledParty = session.getCdpn();

		if(calledParty == null) {
			return false;
		}

		if(UserDetailsManager.getInstance().isSipUser(calledParty)) {
			session.setCdpn(UserDetailsManager.getInstance().getPhoneNumberFromSipUser(calledParty));
		}

		return true;
	}

	public String getName() {
		return "FetchCalledPartySipUserAction";
	}
}
