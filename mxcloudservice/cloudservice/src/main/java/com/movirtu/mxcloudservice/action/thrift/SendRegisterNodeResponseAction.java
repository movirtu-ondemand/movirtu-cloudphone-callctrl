package com.movirtu.mxcloudservice.action.thrift;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public class SendRegisterNodeResponseAction extends BaseThriftAction{

	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		ConnectorAction action = new ConnectorAction(ConnectorActionType.REGISTER_NODE_RESPONSE);
		action.setSession(session);
		action.setMap(getHashMap());
		getConnector().getActionQueue().add(action);
		return true;
	}

}
