package com.movirtu.mxcloudservice.action.ari;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class ContinueAction extends BaseAriAction{

	private Logger logger = Logger.getLogger(ContinueAction.class);
	public boolean perform(final Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		/*getAriConnector().getARI().endpoints().get("PJSIP", 
													"9100", 
													new AriCallback<Endpoint>(){

														public void onFailure(
																RestException arg0) {
															// TODO Auto-generated method stub
															logger.info(arg0.toString());
														}

														public void onSuccess(
																Endpoint arg0) {
															// TODO Auto-generated method stub
															logger.info("Endpoint details");
															logger.info("resource="+arg0.getResource());
															logger.info("state="+arg0.getState());
															logger.info("technology="+arg0.getTechnology());
														}
			
		});
		*/
        getAriConnector().getARI().channels().continueInDialplan(session.getChannelId(), 
        														 session.getContext(),
        														 session.getCdpn(), 
        														 (int) session.getPriority()+1, 
        														 new AriCallback<Void>() {
                        public void onSuccess(Void result) {
                        // Let's do something with the returned value
                        logger.info("continueAction successful");
                    }

                    public void onFailure(RestException e) {
                        logger.info( "continueAction failed "+e);
                        e.printStackTrace();
                        onError(session);
                    }
                });
        return true;

	}

}
