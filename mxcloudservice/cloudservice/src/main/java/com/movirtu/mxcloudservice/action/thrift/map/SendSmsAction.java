package com.movirtu.mxcloudservice.action.thrift.map;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.thrift.BaseThriftAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public class SendSmsAction extends BaseThriftAction{

	public boolean perform(BaseSession session) throws Exception, RestException {
		if(checkConstraints(session)) {
			ConnectorAction action = new ConnectorAction(ConnectorActionType.SEND_SMS);
			action.setSession(session);
			long id = getConnector().getCache().generateConnectorSessionId(session.getSessionId());
			action.setConnecterSessionId(id);
			//action.setConnecterSessionId(getConnector().getCache().getConnectorSessionIdFromServiceSessionId(session.getSessionId()));
			getConnector().getActionQueue().add(action);
			return true;
		}
		return false;
	}
}
