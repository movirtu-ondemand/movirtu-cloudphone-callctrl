package com.movirtu.mxcloudservice.action.sms;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.ari.BaseAriAction;
import com.movirtu.mxcloudservice.service.Session;

public class ParseAndCreateOutgoingSmsAction extends BaseAriAction {

	private Logger logger = Logger.getLogger(ParseAndCreateOutgoingSmsAction.class);

	public boolean perform(Session session) throws Exception, RestException {
		System.out.println("ParseAndCreateOutgoingSmsAction: Hello sms state machine");
		return true;
	}
}
