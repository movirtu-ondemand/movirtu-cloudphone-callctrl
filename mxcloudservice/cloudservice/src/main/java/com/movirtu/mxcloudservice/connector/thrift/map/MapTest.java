package com.movirtu.mxcloudservice.connector.thrift.map;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;

import com.movirtu.mxcloudservice.connector.thrift.map.types.serviceAppType;

public class MapTest {
	

	static mapjavagw.Client client;
	private static TServerSocket serverTransport;
	private static TSimpleServer server;
	private static String args[];
	private static Thread t;
	private static Logger logger = Logger.getLogger(MapTest.class);
	public static class Handler implements mapjavainterface.Iface {
		
		public static final Handler instance = new Handler();
		
		
		@Override
		public void respMissedCallDetails(int service_id, long session_id,
				String coding_scheme, String missed_call_data,
				String diverted_msisdn, Map<String, String> extension)
				throws TException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reqUpdateVLRDetails(int service_id, long session_id,
				String vimsi, Map<String, String> extension) throws TException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void respvMSRN(int service_id, long session_id, String vmsrn,
				short error, serviceAppType type, Map<String, String> extension)
				throws TException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reqUSSD(int service_id, long session_id,
				String coding_scheme, String data_string, String msisdn,
				Map<String, String> extension) throws TException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void respInitiateUSSDSession(int service_id, long session_id,
				String coding_scheme, String data_string, String msisdn,
				short error, Map<String, String> extension) throws TException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void respIncommingSmsData(int service_id, long session_id,
				short error, Map<String, String> extension) throws TException {
			// TODO Auto-generated method stub
			System.out.println("received respIncommingSmsData session_id = "+session_id+
					", service_id="+service_id+",error="+error);

		}

		@Override
		public void reqOutgoingSmsData(int service_id, long session_id,
				String calling_party, String called_party,
				String coding_scheme, String data_string,
				Map<String, String> extension) throws TException {
			// TODO Auto-generated method stub
			System.out.println("received reqOutgoingSmsData session_id = "+session_id+
					", service_id="+service_id+",calling_party="+calling_party
					+",called_party="+called_party+",coding_scheme="+coding_scheme
					+",data_string="+data_string);
			//client.respOutgoingSmsData(service_id, session_id, (short) 0, extension);
		}

		@Override
		public void registerNodeRequest(Map<String, String> extension)
				throws TException {
			// TODO Auto-generated method stub
			System.out.println("received registerNodeRequest");
			if(extension!=null) {
				String val = extension.get("ServiceId");
				if(val!=null)
					System.out.println("ServiceId="+val);
			}
			
			TFramedTransport transport = new TFramedTransport(new TSocket(args[0],Integer.parseInt(args[1])));
			try {
				transport.open();
			} catch (TTransportException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			TBinaryProtocol protocol = new TBinaryProtocol(transport);
			client = new mapjavagw.Client(protocol);
			client.registerNodeResponse(new HashMap<String,String>());
			System.out.println("RegisterNodeResponse send");
			//t.start();
		}

		@Override
		public void registerNodeResponse(Map<String, String> extension)
				throws TException {
			// TODO Auto-generated method stub
			System.out.println("received registerNodeResponse");
		}

		@Override
		public void serviceTypeResponse(int service_id, long session_id,
				serviceAppType toss, Map<String, String> extension)
				throws TException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void subscriberInfoReq(int service_id, long session_id,
				String msisdn, Map<String, String> extension) throws TException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reqSriSm(int service_id, long session_id, String msisdn,
				Map<String, String> extension) throws TException {
			// TODO Auto-generated method stub
			
		}
	}
	
	private static boolean tryToConnect() {
		TSocket transport = new TSocket(args[0],Integer.parseInt(args[1]));
		try {
			transport.open();
			TBinaryProtocol protocol = new TBinaryProtocol(transport);
			client = new mapjavagw.Client(protocol);
			return true;
		} catch (TTransportException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
	}
	
	public static void main(final String args[]) throws TException, NumberFormatException, UnknownHostException, IOException {
		final float cps = Float.parseFloat(args[3]);
		MapTest.args = args;
		
		t = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				HashMap<String,String> map = new HashMap<String,String>();
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				while(!tryToConnect()){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				try {
					long cnt=0;
					client.registerNodeRequest(map);
					int dur;
					if(cps<=0)
						dur = 1000;
					else
						dur = (int) (1000.0/cps);
					while(true) {
						Thread.sleep(dur);
						try {
						client.reqIncommingSmsData(0, cnt++, "01111222233334444", "0", "hello there !!", "99101"+(cnt%10000), "24062006", new HashMap<String,String>());
						} catch(TException e){

							while(!tryToConnect()){
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
							client.registerNodeRequest(map);
						}
					}
				} catch (TException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});

		
		
		//t.start();
		
		serverTransport =new TServerSocket(Integer.parseInt(args[2]));
		
		mapjavainterface.Processor processor = new mapjavainterface.Processor(Handler.instance); 
		
		server = new TSimpleServer(new Args(serverTransport).processor(processor).transportFactory(new TFramedTransport.Factory()).protocolFactory(new TBinaryProtocol.Factory()));
		
		System.out.println("Waiting for messages");
		
		server.serve();
		/*TProtocolFactory protocolFactory = new TBinaryProtocol.Factory();
		TTransportFactory transportFactory = new TFramedTransport.Factory();
		TServerTransport  serverTransport;

		mapjavainterface.Processor processor = new mapjavainterface.Processor(Handler.instance); 
		
		
		ServerSocket serverSocket_ = new ServerSocket(Integer.parseInt(args[2]), 10,
		        InetAddress.getAllByName(null)[0]);
		    // Prevent 2MSL delay problem on server restarts
		serverSocket_.setReuseAddress(true);
		serverTransport = new TServerSocket(serverSocket_, 100);
		TThreadPoolServer.Args serverArgs = new TThreadPoolServer.Args(serverTransport);
		serverArgs.processor(processor).transportFactory(transportFactory)
		      .protocolFactory(protocolFactory);
		server = new TThreadPoolServer(serverArgs);  
		System.out.println("Waiting for messages on "+args[2]);
		server.serve();*/
	}
}
