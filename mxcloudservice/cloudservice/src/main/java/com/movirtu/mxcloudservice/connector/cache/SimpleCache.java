package com.movirtu.mxcloudservice.connector.cache;

import java.util.concurrent.ConcurrentHashMap;

public class SimpleCache {

	protected ConcurrentHashMap<Long, String> connectorToSessionMap = new ConcurrentHashMap<Long, String>();
	protected ConcurrentHashMap<String, Long> sessionToConnectorMap = new ConcurrentHashMap<String, Long>();
	private long counter=0;
	
	public String generateServiceSessionId(String connectorName, long id) {
		String str = connectorName + id;
		sessionToConnectorMap.put(str, id);
		connectorToSessionMap.put(id, str);
		return str;
	}
	
	public long generateConnectorSessionId(String sessionId) {
		counter++;
		sessionToConnectorMap.put(sessionId, counter);
		connectorToSessionMap.put(counter, sessionId);
		return counter;
	}
	
	public long getConnectorSessionIdFromServiceSessionId(String sessionId) {
		return sessionToConnectorMap.get(sessionId);
	}
	
	public void removeFromMaps(String sessionId) {
		long id = sessionToConnectorMap.get(sessionId);
		removeFromMaps(sessionId, id);
	}
	
	public void removeFromMaps(String sessionId, long id) {
		sessionToConnectorMap.remove(sessionId);
		connectorToSessionMap.remove(id);
	}
	
	public String getServiceSessionIdFromConnectorSessionId(Long id) {
		return connectorToSessionMap.get(id);
	}
	
	public void clear() {

		sessionToConnectorMap.clear();
		connectorToSessionMap.clear();
		counter = 0;
	}

	public long size() {
		// TODO Auto-generated method stub
		return sessionToConnectorMap.size()+sessionToConnectorMap.size();
	}
	
	
}
