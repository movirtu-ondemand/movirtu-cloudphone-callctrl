package com.movirtu.mxcloudservice.dao.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.movirtu.mxcloudservice.Application;
import com.movirtu.mxcloudservice.dao.SSIDRecord;


public class SSIDRecordDaoManager {

	private static String config;
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;

	private Logger logger = Logger.getLogger(SSIDRecordDaoManager.class);

	private static SSIDRecordDaoManager instance; //= new UserDetailsManager();

	private SSIDRecordDaoManager(String config) {

		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			configuration.configure(Application.getXmlPath()+"/"+config);
			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}

	}

	public static void setDatabaseConfigFile(String sConfig) {
		config = sConfig;
	}

	public static SSIDRecordDaoManager getInstance() {
		if(instance == null){
			synchronized(UserDetailsManager.class){
				if(instance == null){
					instance = new SSIDRecordDaoManager(config);
				}
			}
		}
		return instance;
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void closeSessionFactory() {
		if (sessionFactory != null)
			sessionFactory.close();
	}

	public ArrayList<SSIDRecord> getAllSSIDRecords() {
		ArrayList<SSIDRecord> ret = new ArrayList<SSIDRecord>();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(SSIDRecord.class);
		cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);;
		List<SSIDRecord> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();

		if(list != null && list.size()>0) {
			for(int i=0;i<list.size();i++) {
				ret.add((SSIDRecord) list.get(i));
			}
			logger.info("SSIDRecord found num of rows = "+list.size());
		} else 
			logger.info("SSIDRecord empty");
		session.getTransaction().commit();
		session.close();
		return ret;
	}

}
