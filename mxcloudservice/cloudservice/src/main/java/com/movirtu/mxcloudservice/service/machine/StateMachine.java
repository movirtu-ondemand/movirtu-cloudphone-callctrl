package com.movirtu.mxcloudservice.service.machine;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.movirtu.mxcloudservice.Application;
import com.movirtu.mxcloudservice.action.Actionable;
import com.movirtu.mxcloudservice.service.machine.factory.StateMachineInTupleFactory;

public class StateMachine {
	private HashMap<StateMachineInTuple, StateMachineOutTuple> stateMap = null;
	private ArrayList<Actionable> defaultActionList;
	private StateMachineOutTuple defaultStateMachineOutTuple = new StateMachineOutTuple();
	private Logger logger = Logger.getLogger(StateMachine.class);
	private ArrayList<String> stateMapList;
    private ArrayList<String> stateTimeoutList=null;
    private HashMap<MachineState, Long> stateTimeout=new HashMap<MachineState,Long>();
    private HashMap<MachineState, MachineEvent> stateTimeoutEvent=new HashMap<MachineState,MachineEvent>();
    
    public MachineEvent getStateTimeoutEvent(MachineState state) {
    	return stateTimeoutEvent.get(state);
    }
    
    public long getStateTimeout(MachineState state) {
    	Long ret = stateTimeout.get(state);
    	return ret!=null?ret:0;
    }
    
    public void init() {
    	initStateTimeouts();
    	initStateMap();
    }
    
    public void initStateTimeouts() {
    	if(stateTimeoutList==null)
    		return;
    	
    	for(int i=0;i<this.stateTimeoutList.size();i++) {
    		String entry=this.stateTimeoutList.get(i);
    		String[] split = entry.split(":");
    		MachineState state = MachineState.fromString(split[0]);
    		Long timeout = Long.parseLong(split[1]);
    		MachineEvent event = MachineEvent.fromString(split[2]);
    		stateTimeout.put(state, timeout);
    		stateTimeoutEvent.put(state, event);
    		logger.info("State Timeout Map: "+state+"=>"+timeout+"=>"+event);
    	}
    }
	public void initStateMap() {
		
		if(stateMap!=null)
			return;
		
		this.stateMap = new HashMap<StateMachineInTuple, StateMachineOutTuple>();
		for(int i=0;i<this.stateMapList.size();i++) {
			String entry = stateMapList.get(i);
			String first = entry.split("=>")[0];
			String second = entry.split("=>")[1];
			first = first.replace('[', ' ').replace(']', ' ').trim();
			MachineState inState = MachineState.fromString(first.split(",")[0]);
			MachineEvent inEvent = MachineEvent.fromString(first.split(",")[1]);
			
			MachineState outState = MachineState.fromString(second.split(",")[0].replace('[', ' ').trim());
			
			ArrayList<Actionable> actionList = new ArrayList<Actionable>();
			String[] actions = second.split(",")[1].replace(']', ' ').trim().split(";");
			for(int j=0;j<actions.length;j++) {
				
				Actionable action = null;
				try {
					
					action = (Actionable) Application.getContext().getBean(actions[j].trim());
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(action==null) {
					System.err.println("Action = "+actions[j]+" not found.");
				} else 
					actionList.add(action);
				
			}
			
			if(inState!=null && inEvent!=null) {
				StateMachineInTuple inTuple = StateMachineInTupleFactory.getInstance().getStateMachineInTuple(inState, inEvent);
				StateMachineOutTuple outTuple = new StateMachineOutTuple();
				outTuple.setActionsList(actionList);
				outTuple.setState(outState);
				this.stateMap.put(inTuple, outTuple);
			} else {
				this.defaultActionList = actionList;
			}
 		}
	}
	public HashMap<StateMachineInTuple, StateMachineOutTuple> getStateMap() {
		return stateMap;
	}

	public void setStateMap(HashMap<StateMachineInTuple, StateMachineOutTuple> stateMap) {
		this.stateMap = stateMap;
	}
	
	public StateMachineOutTuple execute(StateMachineInTuple in) {
		//logger.info("StateMachine (state,event) = ("+in.getState()+","+in.getEvent()+")");
		
		if(stateMap.containsKey(in)) {
			return stateMap.get(in);
		} else {
			defaultStateMachineOutTuple.setState(in.getState());
			return defaultStateMachineOutTuple;
		}
	}

	public ArrayList<Actionable> getDefaultActionList() {
		return defaultActionList;
	}

	public void setDefaultActionList(ArrayList<Actionable> defaultActionList) {
		this.defaultActionList = defaultActionList;
		defaultStateMachineOutTuple.setActionsList(defaultActionList);
	}

	public ArrayList<String> getStateMapList() {
		return stateMapList;
	}

	public void setStateMapList(ArrayList<String> stateMapList) {
		this.stateMapList = stateMapList;
	}
	public ArrayList<String> getStateTimeoutList() {
		return stateTimeoutList;
	}
	public void setStateTimeoutList(ArrayList<String> stateTimeoutList) {
		this.stateTimeoutList = stateTimeoutList;
	}
}
