package com.movirtu.mxcloudservice.dao.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.movirtu.mxcloudservice.Application;
import com.movirtu.mxcloudservice.dao.UserDetails;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class UserDetailsManager extends BaseDaoManager{

	private static String config;
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;

	private Logger logger = Logger.getLogger(UserDetailsManager.class);

	private static UserDetailsManager instance; //= new UserDetailsManager();

	private UserDetailsManager(String config) {

		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			
			configuration.configure(Application.getXmlPath()+"/"+config);
			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}

	}

	public static void setDatabaseConfigFile(String sConfig) {
		config = sConfig;
	}

	public static UserDetailsManager getInstance() {
		if(instance == null){
			synchronized(UserDetailsManager.class){
				if(instance == null){
					instance = new UserDetailsManager(config);
				}
			}
		}
		return instance;
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void closeSessionFactory() {
		if (sessionFactory != null)
			sessionFactory.close();
	}

	public ArrayList<String> getAllSipUser() {
		ArrayList<String> ret = new ArrayList<String>();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(UserDetails.class);
		cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);;
		List<UserDetails> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();

		//List<UserDetails> list = session.createQuery("from UserDetails").list();

		if(list != null && list.size()>0) {
			for(int i=0;i<list.size();i++) {
				ret.add(((UserDetails) list.get(i)).getSipUser());
			}
			logger.info("UserDetails found num of rows = "+list.size());
		} else 
			logger.info("UserDetails empty");
		session.getTransaction().commit();
		session.close();
		return ret;
	}

	public class FetchUserDetails implements Callable<UserDetails>{

		String columnName;
		String value;
		com.movirtu.mxcloudservice.service.Session session;
		MachineEvent successEvent;
		MachineEvent failureEvent;
		
		public FetchUserDetails(String columnName, String value, com.movirtu.mxcloudservice.service.Session session, MachineEvent successEvent, MachineEvent failureEvent) {
			this.columnName = columnName;
			this.value = value;
			this.session = session;
			this.successEvent = successEvent;
			this.failureEvent = failureEvent;
		}
		@Override
		public UserDetails call() throws Exception {
			// TODO Auto-generated method stub
			UserDetails ret = fetchUserDetails(columnName, value);
			if(ret!=null) {
				session.getService().execute(session, successEvent);
				return ret;
			} else {
				session.getService().execute(session, failureEvent);
				return null;
			}
		}
	}
	
	public Future<UserDetails> fetchUserDetailsAsync(String columnName, String value, com.movirtu.mxcloudservice.service.Session session, MachineEvent successEvent, MachineEvent failureEvent) {
		return executor.submit(new FetchUserDetails(columnName,value,session, successEvent, failureEvent));
	}
	
	public UserDetails fetchUserDetails(String columnName, String value) {
		UserDetails ret = null;
		
		if(columnName==null || value==null){
			logger.error("Cannot read database because columnName="+columnName+",value="+value);
			return null;
		}
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			Criteria cr = session.createCriteria(UserDetails.class);
			cr.add(Restrictions.eq(columnName, value));
			List<UserDetails> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
			if(list != null && list.size()>0) {
				UserDetails ud = (UserDetails) list.get(0);
				ret = ud;
				logger.info("User found with "+columnName+" = ["+value+"]=>"+ ud);
			} else 
				logger.info("User not found for "+columnName+" = ["+value+"]");
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error reading database "+e.getMessage()+", columnName="+columnName+",value="+value);
		} finally {
			session.getTransaction().commit();
			session.close();
		}
		return ret;
	}
	
	public Future<UserDetails> getUserDetailsFromImsiAsync(final String imsi,final com.movirtu.mxcloudservice.service.Session session, final MachineEvent successEvent, final MachineEvent failureEvent) {
		return executor.submit(new Callable<UserDetails>(){
			public UserDetails call() {
				UserDetails ret = getUserDetailsFromImsi(imsi);
				if(ret!=null) {
					session.getService().execute(session, successEvent);
					return ret;
				} else {
					session.getService().execute(session, failureEvent);
					return null;
				}
			}
		});
	}
	public UserDetails getUserDetailsFromImsi(String imsi) {
		UserDetails ret = null;
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(UserDetails.class);
		cr.add(Restrictions.eq("imsi", imsi));
		List<UserDetails> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
		if(list != null && list.size()>0) {

			UserDetails ud = (UserDetails) list.get(0);
			ret = ud;
			logger.info("User found with imsi = ["+imsi+"]=>"+ ud);
		} else 
			logger.info("User not found for imsi = ["+imsi+"]");
		session.getTransaction().commit();
		session.close();
		return ret;
	}
	
	public Future<UserDetails> getUserDetailsFromMsrnAsync(final String msrn,final com.movirtu.mxcloudservice.service.Session session, final MachineEvent successEvent, final MachineEvent failureEvent) {
		return executor.submit(new Callable<UserDetails>(){
			public UserDetails call() {
				UserDetails ret = getUserDetailsFromMsrn(msrn);
				if(ret!=null) {
					session.getService().execute(session, successEvent);
					return ret;
				} else {
					session.getService().execute(session, failureEvent);
					return null;
				}
			}
		});
	}
	
	public UserDetails getUserDetailsFromMsrn(String msrn) {
		UserDetails ret = null;
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(UserDetails.class);
		cr.add(Restrictions.eq("msrn", msrn));
		List<UserDetails> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
		if(list != null && list.size()>0) {

			UserDetails ud = (UserDetails) list.get(0);
			ret = ud;
			logger.info("User found with msrn = ["+msrn+"]=>" + ud);
		} else 
			logger.info("User not found for msrn = ["+msrn+"]");
		session.getTransaction().commit();
		session.close();
		return ret;
	}
	
	public UserDetails getUserDetailsFromSipUser(String sipUser) {
		UserDetails ret=null;
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(UserDetails.class);
		cr.add(Restrictions.eq("sipUser", sipUser));
		List<UserDetails> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
		if(list != null && list.size()>0) {

			UserDetails ud = (UserDetails) list.get(0);
			ret = ud;
			logger.info("User found with sipUser = ["+sipUser+"] and phoneNumber=["+ret+"]");
		} else 
			logger.info("User not found for sipUser = ["+sipUser+"]");
		session.getTransaction().commit();
		session.close();
		return ret;
	}
	
	public String getPhoneNumberFromSipUser(String sipUser) {
		String ret = "Unknown";
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(UserDetails.class);
		cr.add(Restrictions.eq("sipUser", sipUser));
		List<UserDetails> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
		if(list != null && list.size()>0) {

			UserDetails ud = (UserDetails) list.get(0);
			ret = ud.getPhoneNumber();
			logger.info("User found with sipUser = ["+sipUser+"] and phoneNumber=["+ret+"]");
		} else 
			logger.info("User not found for sipUser = ["+sipUser+"]");
		session.getTransaction().commit();
		session.close();
		return ret;
	}

	public String getSipUserFromPhoneNumber(String phoneNumber) {
		String ret = phoneNumber.trim();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(UserDetails.class);
		cr.add(Restrictions.eq("phoneNumber", phoneNumber));
		List<UserDetails> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
		if(list != null && list.size()>0) {
			UserDetails ud = (UserDetails) list.get(0);
			ret = ud.getSipUser();
			logger.info("User found with sipUser = ["+ret+"] and phoneNumber=["+phoneNumber+"]");
		} else 
			logger.info("User not found with phoneNumber = ["+phoneNumber+"]");
		session.getTransaction().commit();
		session.close();
		return ret;
	}


	public String getPhoneNumberFromSipUser2(String sipUser) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		UserDetails d1 = new UserDetails();
		d1.setPhoneNumber("9910199554");
		d1.setSipUser("9999");
		d1.setUserName("durgesh1");
		int userId = (Integer) session.save(d1);
		//session.get(UserDetails.class, arg1)
		session.getTransaction().commit();

		UserDetails d2 = (UserDetails) session.get(UserDetails.class, userId);
		System.out.println("SipUser Name = "+d2.getSipUser());
		session.close();

		return d2.getPhoneNumber();
	}

	public boolean isSipUser(String number) {
		boolean isSipUser = false;
		Session session = sessionFactory.openSession();
		Transaction tx=null;
		try {
			tx=session.beginTransaction();
			Criteria cr = session.createCriteria(UserDetails.class);
			cr.add(Restrictions.eq("sipUser", number));
			List<UserDetails> list = cr.list();
			if(list != null && list.size()>0) {
				isSipUser=true;
				logger.info(number+ " is a sip user");
			} else {
				isSipUser=false;
				logger.info(number+ " is not a sip user");
			}
			tx.commit();
		}
		catch (Exception ex) {
			if (tx!=null) {
				tx.rollback();
			}
			ex.printStackTrace();
			logger.error("Exception in isSipUser for number="+number , ex);
		}
		finally {
			session.close();
		}
		System.out.println(number+ " is a sip user=["+isSipUser +"]");
		return isSipUser;
	}

	public boolean isMsisdn(String number) {
		boolean isMsisdn = false;
		Session session = sessionFactory.openSession();
		Transaction tx=null;
		try {
			tx=session.beginTransaction();
			Criteria cr = session.createCriteria(UserDetails.class);
			cr.add(Restrictions.eq("phoneNumber", number));
			List<UserDetails> list = cr.list();
			if(list != null && list.size()>0) {
				isMsisdn=true;
				logger.info(number+ " is msisdn number");
			} else {
				isMsisdn=false;
				logger.info(number+ " is not msisdn number");
			}
			tx.commit();
		}
		catch (Exception ex) {
			if (tx!=null) {
				tx.rollback();
			}
			ex.printStackTrace();
			logger.error("Exception in isMsisdn for number="+number , ex);
		}
		finally {
			session.close();
		}
		System.out.println(number+ " is a msisdn number=["+isMsisdn +"]");
		return isMsisdn;
	}

	/*	public Collection<UserDetails> fetchAllUserDetails() throws DataAccessException {
		return (Collection<UserDetails>) template.find("from Person");
	}

	public UserDetails fetchUserDetailsWithSipUser(String sipUser) throws DataAccessException {
		Collection<UserDetails> ret =  (Collection<UserDetails>) template.find("from UserDetails u where u.sipUser = ?",sipUser);
		if(ret == null || ret.isEmpty()) 
			return null;
		else 
			return (UserDetails) ret.toArray()[0];

	}*/
}
