package com.movirtu.mxcloudservice.connector.ari;

import ch.loway.oss.ari4java.generated.ApplicationReplaced;
import ch.loway.oss.ari4java.generated.ChannelDestroyed;
import ch.loway.oss.ari4java.generated.ChannelHangupRequest;
import ch.loway.oss.ari4java.generated.ChannelStateChange;
import ch.loway.oss.ari4java.generated.EndpointStateChange;
import ch.loway.oss.ari4java.generated.Message;
import ch.loway.oss.ari4java.generated.StasisStart;

import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.SessionManager;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;
import com.movirtu.mxcloudservice.service.machine.MachineState;

public class CallAriConnector extends AriConnector{

	@Override
	public String generateSessionId(String id) {
		// TODO Auto-generated method stub
		return getName()+id;
	}

	
	public void onEvent(Message m) {
		if(m instanceof StasisStart) {
			Session session = (Session) SessionManager.getInstance().getOrCreateSession(generateSessionId(((StasisStart) m).getChannel().getId()), getSessionFactory(), service);
			//Session session = SessionManager.getInstance().getSession(getName()+((StasisStart) m).getChannel().getId());
			//if(((StasisStart) m).getArgs().size()>0)
	        //	session.setCdpn(((StasisStart) m).getArgs().get(0).trim());
			session.setChannelId(((StasisStart) m).getChannel().getId());
			session.setPriority(((StasisStart) m).getChannel().getDialplan().getPriority());
			session.setContext(((StasisStart) m).getChannel().getDialplan().getContext());
			session.setCdpn(((StasisStart) m).getChannel().getDialplan().getExten());
			session.setCgpn(((StasisStart) m).getChannel().getCaller().getNumber());
			session.setOrigCdpn(session.getCdpn());
			session.setOrigCgpn(session.getCgpn());
			session.setState(MachineState.NULL);
			getService().execute(session, MachineEvent.STASIS_START);
		} else if(m instanceof ChannelDestroyed) {
			Session session = (Session) SessionManager.getInstance().getSession(generateSessionId(((ChannelDestroyed) m).getChannel().getId()));
			if(session!=null) getService().execute(session, MachineEvent.DESTROYED);
		} else if(m instanceof ChannelHangupRequest) {
			Session session = (Session) SessionManager.getInstance().getSession(generateSessionId(((ChannelHangupRequest) m).getChannel().getId()));
			if(session!=null) getService().execute(session, MachineEvent.HANGUP);
		} else if(m instanceof ChannelStateChange) {
			Session session = (Session) SessionManager.getInstance().getSession(generateSessionId(((ChannelStateChange) m).getChannel().getId()));
			System.out.println(((ChannelStateChange) m).getChannel().getState());
			if(((ChannelStateChange) m).getChannel().getState().compareToIgnoreCase("up")==0) {
				if(session!=null) getService().execute(session, MachineEvent.ANSWERED);	
			} else if(((ChannelStateChange) m).getChannel().getState().compareToIgnoreCase("ringing")==0) {
				if(session!=null) getService().execute(session, MachineEvent.RINGING);	
			}
		} else if (m instanceof EndpointStateChange) {
			EndpointStateChange event = (EndpointStateChange) m;
			logger.info("Resource:"+event.getEndpoint().getResource()
					    +",Technology:"+event.getEndpoint().getTechnology()
					    +",State:"+event.getEndpoint().getState());
			//event.getEndpoint().
		} else if (m instanceof ApplicationReplaced) {
			ApplicationReplaced event = (ApplicationReplaced) m;
			logger.info(":-( Application Replaced:"+event.toString());
			logger.info(":-( We may no longer be getting further events.");
		}
	}


}
