package com.movirtu.mxcloudservice.connector.event;

import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class SmsResponseEvent extends Event{

	private int serviceId;
	private long connectorSessionId;
	private short errorCode;
	
	public SmsResponseEvent(String str, MachineEvent event, int serviceId, long connectorSessionId, short errorCode) {
		super(str, event);
		// TODO Auto-generated constructor stub
		setServiceId(serviceId);
		setConnectorSessionId(connectorSessionId);
		setErrorCode(errorCode);
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public long getConnectorSessionId() {
		return connectorSessionId;
	}

	public void setConnectorSessionId(long connectorSessionId) {
		this.connectorSessionId = connectorSessionId;
	}

	public short getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(short errorCode) {
		this.errorCode = errorCode;
	}

}
