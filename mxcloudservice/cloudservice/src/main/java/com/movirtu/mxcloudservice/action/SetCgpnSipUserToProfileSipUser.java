package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class SetCgpnSipUserToProfileSipUser extends CallBaseAction{

	@Override
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
			String sipUser = null;
			if(session.getCgpnProfile()!=null) {
				sipUser = session.getCgpnProfile().getSipUser();
			}
			if(sipUser!=null) {
				session.setCallingPartySipUser(sipUser);
			} else {
				if(proceedEvenWithFailure())
					return true;
				else
					return false;
			}
			return true;
		}
}
