package com.movirtu.mxcloudservice.util;

public interface ExpireHandler {
	public void onExpire(String id, Object obj);
}
