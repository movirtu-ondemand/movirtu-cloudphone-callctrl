package com.movirtu.mxcloudservice.service.machine;

import java.util.ArrayList;

import com.movirtu.mxcloudservice.action.Actionable;

public class StateMachineOutTuple {
	private MachineState state;
	private ArrayList<Actionable> actionsList;
	public MachineState getState() {
		return state;
	}
	public void setState(MachineState state) {
		this.state = state;
	}
	public ArrayList<Actionable> getActionsList() {
		return actionsList;
	}
	public void setActionsList(ArrayList<Actionable> actionsList) {
		this.actionsList = actionsList;
	}
	
}
