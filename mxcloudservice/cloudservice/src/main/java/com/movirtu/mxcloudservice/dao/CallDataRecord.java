package com.movirtu.mxcloudservice.dao;

public class CallDataRecord {
	private int id;
	private int transactionType;
	private String transactionDate;
	private String transactionTime;
	private String otherParty;
	private String imsi;
	private String imei;
	private String msisdn;
	private String startOfChargingDate;
	private String chargingDuration;
	private String startOfChargingTime;
	private String cellId;
	private String basicService;
	private String sequenceNumber;
	
	public String toString() {
		return "CDR:id="+id+
				",transactionType="+transactionType+
				",transactionDate="+transactionDate+
				",transactionTime="+transactionTime+
				",otherParty="+otherParty+
				",imsi="+imsi+
				",imei="+imei+
				",msisdn="+msisdn+
				",startOfChargingDate="+startOfChargingDate+
				",chargingDuration="+chargingDuration+
				",startOfChargingTime="+startOfChargingTime+
				",cellId="+cellId+
				",basicService="+basicService+
				",sequenceNumber="+sequenceNumber;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public int getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getOtherParty() {
		return otherParty;
	}
	public void setOtherParty(String otherParty) {
		this.otherParty = otherParty;
	}
	public String getStartOfChargingDate() {
		return startOfChargingDate;
	}
	public void setStartOfChargingDate(String startOfChargingDate) {
		this.startOfChargingDate = startOfChargingDate;
	}
	public String getChargingDuration() {
		return chargingDuration;
	}
	public void setChargingDuration(String chargingDuration) {
		this.chargingDuration = chargingDuration;
	}
	public String getStartOfChargingTime() {
		return startOfChargingTime;
	}
	public void setStartOfChargingTime(String startOfChargingTime) {
		this.startOfChargingTime = startOfChargingTime;
	}
	public String getCellId() {
		return cellId;
	}
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getBasicService() {
		return basicService;
	}
	public void setBasicService(String basicService) {
		this.basicService = basicService;
	}
}
