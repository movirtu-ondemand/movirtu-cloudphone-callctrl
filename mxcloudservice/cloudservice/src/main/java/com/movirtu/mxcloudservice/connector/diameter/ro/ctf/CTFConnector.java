package com.movirtu.mxcloudservice.connector.diameter.ro.ctf;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.Message;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwCCRBuilder;
import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwServerCCA;
import com.movirtu.diameter.charging.apps.common.PrepaidServiceProcessingDiameterMessages;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.mxcloudservice.connector.GenericConsumer;
import com.movirtu.mxcloudservice.connector.diameter.ro.OnlineChargingConnector;
import com.movirtu.mxcloudservice.connector.event.DiameterEvent;
import com.movirtu.mxcloudservice.connector.thrift.cdr.CDRClient;
import com.movirtu.mxcloudservice.dao.manager.DataUserProfileDaoManager;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class CTFConnector extends OnlineChargingConnector implements PrepaidServiceProcessingDiameterMessages{
	 
	private B2BGwServerCCA cca;
	Logger logger = Logger.getLogger(CTFConnector.class);
	

	protected void createClient() {
		// TODO Auto-generated method stub
		actionConsumer = new GenericConsumer(outQueue, this, getActionExecutor(),new CTFConnectorActionConsumer(this));
		
	}

	protected void createServer() throws Exception {
		// TODO Auto-generated method stub
		eventConsumer = new GenericConsumer(inQueue, this, getEventExecutor() , new CTFConnectorEventConsumer(this));
		cca = new B2BGwServerCCA(this);
		//cca.init(new FileInputStream(new File(getConfigFileName())),"SERVER1");
		cca.init(new FileInputStream(new File(new URI("file:"+getConfigFileName()))),"SERVER1");
		cca.start();
		getService().init();
		DataUserProfileDaoManager.getInstance().testConnection();
		CDRClient.sendCDR(new HashMap());
	}

	public B2BGwServerCCA getClient() {
		return cca;
	}
	
	public void serviceProcessingCCR(TransactionInfo obj) {
		// TODO Auto-generated method stub
		logger.info("Received CCR from CTF");
		System.out.println("Received CCR from CTF");
		
		if(obj.recordType==B2BGwCCRBuilder.CC_REQUEST_TYPE_INITIAL)
			getEventQueue().add(new DiameterEvent("", MachineEvent.CCR_INIT, obj));
		else if(obj.recordType==B2BGwCCRBuilder.CC_REQUEST_TYPE_INTERIM)
			getEventQueue().add(new DiameterEvent("", MachineEvent.CCR_INTERIM, obj));
		else if(obj.recordType==B2BGwCCRBuilder.CC_REQUEST_TYPE_TERMINATE)
			getEventQueue().add(new DiameterEvent("", MachineEvent.CCR_TERM, obj));
	}

	public void serviceProcessingCCA(TransactionInfo obj) {
		// TODO Auto-generated method stub
		logger.info("we should not receive CCA from CTF");
		try {
			obj.display();
		} catch (InternalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sessionSupervisionTimerExpiredNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void serviceProcessingRAR(TransactionInfo obj) {
		// TODO Auto-generated method stub
		
	}

	public void serviceProcessingRAA(TransactionInfo obj) {
		// TODO Auto-generated method stub
		
	}

	public void sessionSupervisionTimerStartedNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void sessionSupervisionTimerRestartedNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void sessionSupervisionTimerStoppedNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void txTimerExpiredNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void grantAccessOnDeliverFailureNotification(String sessionId,
			Message request) {
		// TODO Auto-generated method stub
		
	}

	public void denyAccessOnDeliverFailureNotification(String sessionId,
			Message request) {
		// TODO Auto-generated method stub
		
	}

	public void grantAccessOnTxExpireNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void denyAccessOnTxExpireNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void grantAccessOnFailureMessageNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void denyAccessOnFailureMessageNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

	public void indicateServiceErrorNotification(String sessionId) {
		// TODO Auto-generated method stub
		
	}

}
