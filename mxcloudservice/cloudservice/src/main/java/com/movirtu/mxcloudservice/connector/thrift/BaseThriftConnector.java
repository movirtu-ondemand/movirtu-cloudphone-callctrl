package com.movirtu.mxcloudservice.connector.thrift;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import com.movirtu.mxcloudservice.connector.AbstractConnector;
import com.movirtu.mxcloudservice.connector.GenericConsumer;
import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.cache.SimpleCache;
import com.movirtu.mxcloudservice.connector.event.Event;
import com.movirtu.mxcloudservice.service.Service;
import com.movirtu.mxcloudservice.service.SessionFactory;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public abstract class BaseThriftConnector extends AbstractConnector{

	private SimpleCache cache = new SimpleCache();
	
	protected SessionFactory sessionFactory;
	protected String name;
	protected String senderIpAddress;
	protected int senderPort;
	protected int receiverPort;
	
	protected TServerTransport serverTransport;
	protected TServer server;
	protected TTransport transport;
	protected TProtocol protocol;
	//protected TSocket socket;
	
	protected BlockingQueue inQueue;
	protected BlockingQueue outQueue;
	
	protected int inQueueSize;
	protected int outQueueSize;
	
	protected Runnable actionConsumer;
	protected Runnable eventConsumer;
	
	protected ExecutorService actionExecutor;
	protected ExecutorService eventExecutor;
	
	protected int actionThreadPoolSize;
	protected int eventThreadPoolSize;
	
	protected Service service;
	
	Logger logger = Logger.getLogger(BaseThriftConnector.class);
	
	public SimpleCache getCache() {
		return cache;
	}

	
	public BlockingQueue<ConnectorAction> getActionQueue() {
		return outQueue;
	}
	
	public BlockingQueue<Event> getEventQueue() {
		return inQueue;
	}
	
	public ExecutorService getActionExecutor() {
		return actionExecutor;
	}
	
	public ExecutorService getEventExecutor() {
		return eventExecutor;
	}
	
	public Service getService() {
		return service;
	}
	
	public void setService(Service service) {
		this.service = service;
	}
	
	public void setActionThreadPoolSize(int actionThreadPoolSize) {
		this.actionThreadPoolSize = actionThreadPoolSize;
		if(actionExecutor==null) {
			actionExecutor = Executors.newFixedThreadPool(actionThreadPoolSize);
		} else {
			actionExecutor.shutdownNow();
			actionExecutor = Executors.newFixedThreadPool(actionThreadPoolSize);
		}
	}
	
	public int getActionThreadPoolSize() {
		return actionThreadPoolSize;
	}
	
	public int getEventThreadPoolSize() {
		return eventThreadPoolSize;
	}
	
	public void setEventThreadPoolSize(int eventThreadPoolSize) {
		this.eventThreadPoolSize = eventThreadPoolSize;
		if(eventExecutor==null) {
			eventExecutor = Executors.newFixedThreadPool(eventThreadPoolSize);
		} else {
			eventExecutor.shutdownNow();
			eventExecutor = Executors.newFixedThreadPool(eventThreadPoolSize);
		}
	}
	
	public int getInQueueSize() {
		return inQueueSize;
	}
	
	public void setInQueueSize(int inQueueSize) {
		this.inQueueSize = inQueueSize;
	}
	

	public int getOutQueueSize() {
		return outQueueSize;
	}
	
	public void setOutQueueSize(int inQueueSize) {
		this.outQueueSize = inQueueSize;
	}
	
	public int getReceiverPort() {
		return receiverPort;
	}
	
	public void setReceiverPort(int receiverPort) {
		this.receiverPort = receiverPort;
	}
	
	public void setSenderPort(int senderPort) {
		this.senderPort = senderPort;
	}
	
	public int getSenderPort() {
		return senderPort;
	}
	
	public void setSenderIpAddress(String senderIpAddress) {
		this.senderIpAddress = senderIpAddress;
	}
	
	public String getSenderIpAddress() {
		return senderIpAddress;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	protected abstract void createClient() throws TTransportException;
	
	protected abstract void createServer() throws TTransportException, UnknownHostException, IOException;
	
	private void initExecutors() {
		if(actionExecutor==null && actionThreadPoolSize!=0)
			actionExecutor = Executors.newFixedThreadPool(actionThreadPoolSize);
		if(eventExecutor==null && eventThreadPoolSize!=0)
			eventExecutor = Executors.newFixedThreadPool(eventThreadPoolSize);
	}
	
	protected void startConsumers() {
		if(actionExecutor!=null) actionExecutor.submit(actionConsumer);
		if(eventExecutor!=null) eventExecutor.submit(eventConsumer);
	}
	
	public void createQueues() {
		inQueue = new ArrayBlockingQueue(inQueueSize);
		outQueue = new ArrayBlockingQueue(outQueueSize);
	}
	
	public void destroyQueues() {
		if(inQueue!=null)
			inQueue.add(new GenericConsumer.EndCommand());
		if(outQueue!=null)
			outQueue.add(new GenericConsumer.EndCommand());
		inQueue = null;
		outQueue = null;
	}
	
	public void closeServer() {
		if(server!=null)
			server.stop();
	}
	
	public void service() {
		// TODO Auto-generated method stub
		try {
			initExecutors();
			createQueues();
			
			createClient();
			createServer();
			
			changeState(State.RUNNING);
			
			onInit();
			startConsumers();
			server.serve();// blocked here..
			
		} catch (TTransportException e) {
			logger.error(e.toString());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			changeState(State.STOPPED);
			closeClient();
			destroyQueues();
			//destroyExecutors();
			onDestroy();
		}
	}
	
	private void closeClient() {
		if(transport!=null) {
			transport.close();
		}
		if(actionExecutor!=null)
			actionExecutor.shutdownNow();
		if(eventExecutor!=null)
			eventExecutor.shutdownNow();
		
		actionExecutor = null;
		eventExecutor = null;
	}
	

	public State getState() {
		// TODO Auto-generated method stub
		return state;
	}

	public void ping() {
		logger.info(getName()+" current cache size = "+getCache().size());
	}

	public void onInit() {
		// TODO Auto-generated method stub
		if(inQueue!=null)
			inQueue.add(new Event(getName(),MachineEvent.INIT));
		
		cache.clear();
	}

	public void onDestroy() {
		// TODO Auto-generated method stub
		if(inQueue!=null)
			inQueue.add(new Event(getName(),MachineEvent.DESTROY));

		cache.clear();
	}
	
	public void onConnectorException(Exception e, Object obj) {
		//e.printStackTrace();
		logger.error(getName()+ " received exception = "+e.getMessage());
		if(obj!=null) {
			if(obj instanceof ConnectorAction) {
				ConnectorAction action = (ConnectorAction) obj;
				inQueue.add(new Event(action.getSession().getSessionId(),MachineEvent.ERROR));
			} else if (obj instanceof Event) {
				logger.fatal("Connector Event Queue has got corrupt. Restart the application to fix it.");
			}
		}
		inQueue.add(new Event(getName(),MachineEvent.REINIT));
		//logger.error(getName()+" is no more serving");
		//closeServer();
	}
	

	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	public abstract void recreateClient() throws TTransportException;
	
}
