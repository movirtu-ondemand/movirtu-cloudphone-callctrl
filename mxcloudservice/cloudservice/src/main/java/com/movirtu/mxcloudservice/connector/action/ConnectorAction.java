package com.movirtu.mxcloudservice.connector.action;

import java.util.HashMap;

import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public class ConnectorAction {

	private long connecterSessionId;
	private ConnectorActionType type;
	private BaseSession session;
	private int errorCode;
	private HashMap<String,String> map;
	
	public ConnectorAction(ConnectorActionType type) {
		// TODO Auto-generated constructor stub
		this.type = type;
	}

	public ConnectorActionType getType() {
		// TODO Auto-generated method stub
		return type;
	}

	public BaseSession getSession() {
		return session;
	}

	public void setSession(BaseSession session) {
		this.session = session;
	}

	public int getErrorCode() {
		// TODO Auto-generated method stub
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public long getConnecterSessionId() {
		return connecterSessionId;
	}

	public void setConnecterSessionId(long connecterSessionId) {
		this.connecterSessionId = connecterSessionId;
	}

	public HashMap<String,String> getMap() {
		return map;
	}

	public void setMap(HashMap<String,String> map) {
		this.map = map;
	}

}
