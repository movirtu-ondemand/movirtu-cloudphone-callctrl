package com.movirtu.mxcloudservice.connector.thrift.map;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.event.Event;
import com.movirtu.mxcloudservice.connector.event.SmsEvent;
import com.movirtu.mxcloudservice.connector.event.SmsResponseEvent;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class MapGatewayHandler implements mapjavagw.Iface {
	
	MapGatewayConnector connector;
	Logger logger = Logger.getLogger(MapGatewayHandler.class);
	
	public MapGatewayHandler(MapGatewayConnector connector) {
		this.connector = connector;
	}

	public void reqMissedCallDetails(int service_id, long session_id,
			String msisdn, Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

	public void respUpdateVLRDetails(int service_id, long session_id,
			String hlr_number, short error, Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		
	}

	public void reqvMSRN(int service_id, long session_id, String imsi,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

	public void reqInitiateUSSDSession(int service_id, long session_id,
			String coding_scheme, String data_string, String msisdn,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

	public void respUSSD(int service_id, long session_id, String coding_scheme,
			String data_string, String msisdn, Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		
	}

	public void reqIncommingSmsData(int service_id, long session_id,
			String imsi, String coding_scheme, String data_string,
			String orig_party_addr, String timestamp,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		//Session sessionId = SessionManager.getInstance().getOrCreateSession(connector.generateSessionId(session_id), connector.getSessionFactory(), connector.getService());
		connector.getEventQueue().add(new SmsEvent("",
												   session_id,
												   MachineEvent.INCOMING_SMS,
												   service_id,
												   imsi,
												   coding_scheme,
												   data_string,
												   orig_party_addr,
												   timestamp));
		
		
	}

	public void respOutgoingSmsData(int service_id, long session_id,
			short error, Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
	    logger.info("received respOutgoingSmsData "
	    		 +"service_id="+service_id
	    		 +",session_id="+session_id
	    		 +",error="+error);
		connector.getEventQueue().add(new SmsResponseEvent("",
				   error!=0 ? MachineEvent.SMS_SENDING_FAILED:MachineEvent.SMS_SENT_SUCCESSFULLY,
				   service_id,
				   session_id,
				   error));
	}

	public void registerNodeRequest(Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		//connector.onReconnect();
		//connector.getActionQueue().add(new ConnectorAction(ConnectorActionType.REGISTER_NODE_RESPONSE));
		connector.getEventQueue().add(new Event(connector.getName(), MachineEvent.REINIT));
	}

	public void registerNodeResponse(Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		connector.getEventQueue().add(new Event(connector.getName(),MachineEvent.INIT_ACK));
	}

	public void serviceTypeRequest(int service_id, long session_id,
			String msisdn, String imsi, Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		
	}

	public void subscriberInfoResp(int service_id, long session_id,
			String msisdn, Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void respSriSm(int service_id, long session_id, String imsi,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}


}
