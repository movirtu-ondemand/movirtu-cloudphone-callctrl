package com.movirtu.mxcloudservice.connector;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.thrift.transport.TTransportException;
import org.testng.log4testng.Logger;

import com.movirtu.mxcloudservice.ApplicationState;
import com.movirtu.mxcloudservice.connector.Connector.State;
import com.movirtu.mxcloudservice.connector.GenericConsumer.EndCommand;
import com.movirtu.mxcloudservice.connector.GenericConsumer.Worker;
import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.event.Event;
import com.movirtu.mxcloudservice.service.Service;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public abstract class AsyncIOConnector extends AbstractConnector{

	private Logger logger =Logger.getLogger(AsyncIOConnector.class);
	
	protected BlockingQueue<Connector.State> applicationQueue = new ArrayBlockingQueue<Connector.State>(2);
	protected BlockingQueue inQueue;
	protected BlockingQueue outQueue;
	
	protected int inQueueSize;
	protected int outQueueSize;
	
	protected Runnable actionConsumer;
	protected Runnable eventConsumer;
	
	protected ExecutorService actionExecutor;
	protected ExecutorService eventExecutor;
	
	protected int actionThreadPoolSize;
	protected int eventThreadPoolSize;
	
	protected Service service;

	public void setName(String name) {
		this.name = name;
	}
	
	public BlockingQueue<ConnectorAction> getActionQueue() {
		return outQueue;
	}
	
	public BlockingQueue<Event> getEventQueue() {
		return inQueue;
	}
	
	public ExecutorService getActionExecutor() {
		return actionExecutor;
	}
	
	public ExecutorService getEventExecutor() {
		return eventExecutor;
	}
	
	public Service getService() {
		return service;
	}
	
	public void setService(Service service) {
		this.service = service;
	}

	public void setActionThreadPoolSize(int actionThreadPoolSize) {
		this.actionThreadPoolSize = actionThreadPoolSize;
		if(actionExecutor==null && actionThreadPoolSize!=0) {
			actionExecutor = Executors.newFixedThreadPool(actionThreadPoolSize);
		} else {
			actionExecutor.shutdownNow();
			actionExecutor = Executors.newFixedThreadPool(actionThreadPoolSize);
		}
	}
	
	public int getActionThreadPoolSize() {
		return actionThreadPoolSize;
	}
	
	public int getEventThreadPoolSize() {
		return eventThreadPoolSize;
	}
	
	public void setEventThreadPoolSize(int eventThreadPoolSize) {
		this.eventThreadPoolSize = eventThreadPoolSize;
		if(eventExecutor==null && eventThreadPoolSize!=0) {
			eventExecutor = Executors.newFixedThreadPool(eventThreadPoolSize);
		} else {
			eventExecutor.shutdownNow();
			eventExecutor = Executors.newFixedThreadPool(eventThreadPoolSize);
		}
	}

	public int getInQueueSize() {
		return inQueueSize;
	}
	
	public void setInQueueSize(int inQueueSize) {
		this.inQueueSize = inQueueSize;
	}
	

	public int getOutQueueSize() {
		return outQueueSize;
	}
	
	public void setOutQueueSize(int inQueueSize) {
		this.outQueueSize = inQueueSize;
	}

	private void initExecutors() {
		if(actionExecutor==null)
			actionExecutor = Executors.newFixedThreadPool(actionThreadPoolSize);
		if(eventExecutor==null)
			eventExecutor = Executors.newFixedThreadPool(eventThreadPoolSize);
	}
	
	protected void startConsumers() {
		
		actionExecutor.submit(actionConsumer);
		eventExecutor.submit(eventConsumer);
	}
	
	public void createQueues() {
		inQueue = new ArrayBlockingQueue(inQueueSize);
		outQueue = new ArrayBlockingQueue(outQueueSize);
	}
	
	public void destroyQueues() {
		if(inQueue!=null)
			inQueue.add(new GenericConsumer.EndCommand());
		if(outQueue!=null)
			outQueue.add(new GenericConsumer.EndCommand());
		inQueue = null;
		outQueue = null;
	}

	public void service() {
		// TODO Auto-generated method stub
		try {
			initExecutors();
			createQueues();
			
			createClient();
			createServer();
			
			changeState(State.RUNNING);
			
			onInit();
			startConsumers();
			
			waitTillApplicationShutdown();
			// blocked here..
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			changeState(State.STOPPED);
			closeClient();
			destroyQueues();
			//destroyExecutors();
			onDestroy();
		}
	}

	protected void waitTillApplicationShutdown() {
		Object obj=null;
		try {
			while(ApplicationState.getInstance().isRunning()) {
				obj = applicationQueue.poll(10000, TimeUnit.MILLISECONDS);
				if(obj!=null) {
					if(obj instanceof Connector.State) {
						throw new Exception("Queue has been terminated.");
					} 
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//connector.onConnectorException(e, obj);
			e.printStackTrace();
		} catch (Exception e) {
			//connector.onConnectorException(e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected abstract void createClient();
	
	protected abstract void createServer() throws Exception;
	
	protected abstract void closeClient();


	public State getState() {
		// TODO Auto-generated method stub
		return state;
	}


	public void ping() {
		// TODO Auto-generated method stub
		
	}


	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	

	public void onInit() {
		// TODO Auto-generated method stub
		if(inQueue!=null)
			inQueue.add(new Event(getName(),MachineEvent.INIT));
	}

	public void onDestroy() {
		// TODO Auto-generated method stub
		if(inQueue!=null)
			inQueue.add(new Event(getName(),MachineEvent.DESTROY));
	}

}
