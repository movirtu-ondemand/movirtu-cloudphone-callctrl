package com.movirtu.mxcloudservice.action.ari;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.generated.Application;
import ch.loway.oss.ari4java.generated.Endpoint;
import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.Session;

public class SubscriberAllEndpointsAction extends BaseAriAction {
	
	private Logger logger = Logger.getLogger(SubscriberAllEndpointsAction.class);
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		ArrayList<String> sipUsers = UserDetailsManager.getInstance().getAllSipUser();
		if(sipUsers.size()>0) {
			for(int i=0;i<sipUsers.size();i++) {
				logger.info("Subscribing for endpoint="+sipUsers.get(i));

		        getAriConnector().getARI().applications().subscribe(getAriConnector().getApplicationName(), "endpoint:PJSIP/"+sipUsers.get(i) , new AriCallback<Application> () {
		                public void onSuccess(Application result) {
		                        // Let's do something with the returned value
		                	  
		                	  /*Iterator i = result.getEndpoint_ids().iterator();
		                	  while(i.hasNext()) {
		                		  logger.info("subscription successful for endpoint = "+i.next());
		                	  }*/
		                      //  logger.info("Application : "+ result + " subscribed successfully");
		                    }

		                    public void onFailure(RestException e) {
		                        logger.warn( "Failure in subscribing to endpoints ");
		                        logger.warn(e);
		                        //e.printStackTrace();
		                    }
		        });
		        
				getAriConnector().getARI().endpoints().get("PJSIP", 
						sipUsers.get(i), 
						new AriCallback<Endpoint>(){

							public void onFailure(
									RestException arg0) {
								// TODO Auto-generated method stub
								logger.info(arg0.toString());
							}

							public void onSuccess(
									Endpoint arg0) {
								logger.info("Resource:"+arg0.getResource()
									    +",Technology:"+arg0.getTechnology()
									    +",State:"+arg0.getState());
								// TODO Auto-generated method stub
								
							}

				});
			}
		} else {
			logger.info("No SipUsers to be monitored.");
		}
		return true;
	}

}
