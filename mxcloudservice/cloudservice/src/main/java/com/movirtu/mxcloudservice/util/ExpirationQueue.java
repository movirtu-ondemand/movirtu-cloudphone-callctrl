package com.movirtu.mxcloudservice.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class ExpirationQueue implements Runnable{
	
	DelayQueue<TimeoutObject> queue = new DelayQueue<TimeoutObject>();
	ConcurrentHashMap<String, TimeoutObject> map = new ConcurrentHashMap<String, TimeoutObject>();
	long id=0;
	
	public ExpirationQueue() {
		new Thread(this).start();;
	}
	
	synchronized private long getNextId() {
		if(id<Long.MAX_VALUE)
			return id++;
		else {
			id=0;
			return id;
		}
	}
	
	public static class EndCommand {
		
	}
	
	public static class TimeoutObject implements Delayed{
		
		private String id;
		private Object object;
		private ExpireHandler handler;
		private long timeout;
		
		public TimeoutObject(String id, Object obj, long timeout, ExpireHandler handler){
			this.setId(id);
			this.setObject(obj);
			this.handler = handler;
			this.timeout = System.currentTimeMillis()+(timeout * 1000);
		}

		@Override
		public int compareTo(Delayed arg0) {
			// TODO Auto-generated method stub
			if(this.timeout<((TimeoutObject)arg0).timeout)
				return -1;
			else if(this.timeout>((TimeoutObject)arg0).timeout)
				return 1;
			return 0;
		}

		@Override
		public long getDelay(TimeUnit unit) {
			return unit.convert(timeout - System.currentTimeMillis(),TimeUnit.MILLISECONDS);
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Object getObject() {
			return object;
		}

		public void setObject(Object object) {
			this.object = object;
		}
		
		public ExpireHandler getHandler() {
			return handler;
		}
	}
	
	public void putOrUpdateTimeout(String id, long timeout, Object obj, ExpireHandler handler) {
		removeTimeout(id);
		TimeoutObject timeoutObj = new TimeoutObject(id, obj, timeout, handler);
		map.put(id, timeoutObj);
		queue.put(timeoutObj);
		
	}
	
	public String putTimeout(long timeout, Object obj, ExpireHandler handler) {
		String id = Long.toString(getNextId());
		TimeoutObject timeoutObj = new TimeoutObject(id, obj, timeout, handler);
		map.put(id, timeoutObj);
		queue.put(timeoutObj);
		return id;
	}
	
	public void removeTimeout(String id) {
		synchronized(id) {
			TimeoutObject obj = map.remove(id);
			if(obj!=null) {
				queue.remove(obj);
			}
		}
	}

	private void onTimeout(TimeoutObject obj) {
		if(obj!=null) {
			synchronized(obj.getId()) {
				map.remove(obj.getId());
			}
			obj.getHandler().onExpire(obj.getId(),obj.getObject());
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Object obj=null;
		try {
			while(true) {
				obj = queue.poll(300, TimeUnit.SECONDS);
				if(obj!=null) {
					if(obj instanceof EndCommand) {
						throw new Exception("Queue has been terminated.");
					} else if(obj instanceof TimeoutObject) {
						onTimeout( (TimeoutObject) obj );
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//e.printStackTrace();
		} catch (Exception e) {
			//connector.onConnectorException(e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
