package com.movirtu.mxcloudservice.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.movirtu.mxcloudservice.service.machine.MachineState;

public class BaseSession {
	private String sessionId;
	private MachineState state;
	private Service service;
	private Map<String, String> nvp;

	public BaseSession() {
		state = MachineState.NULL;
		nvp = new LinkedHashMap<String, String>();
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public MachineState getState() {
		return state;
	}

	public void setState(MachineState state) {
		this.state = state;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Map<String, String> getNvp() {
		return nvp;
	}

	public void setNvp(Map<String, String> nvp) {
		this.nvp = nvp;
	}
}
