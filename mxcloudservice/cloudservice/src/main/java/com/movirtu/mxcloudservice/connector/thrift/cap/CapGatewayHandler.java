package com.movirtu.mxcloudservice.connector.thrift.cap;

import java.util.Map;

import org.apache.thrift.TException;

public class CapGatewayHandler implements mxcapservice.Iface{

	CapGatewayConnector connector;

	public CapGatewayHandler(CapGatewayConnector connector) {
		this.connector = connector;
	}
	
	@Override
	public void isSmsAllowedResponse(long session_id, boolean is_allowed,
			byte cause, Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectInAssistMode(long session_id, String assisting_ip,
			String scf_id, String correlation_id, Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playAnnouncementWithoutAssist(long session_id,
			String resource_address, String announcement_id,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectCall(long session_id, String destination_number,
			String calling_party_number, String service_interaction_indicators,
			String original_called_party_id, String calling_party_category,
			String redirection_party_id, String redirection_info,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void continueCall(long session_id, Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finishCall(long session_id, Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void genericResponse(long session_id, Map<String, String> extension)
			throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playTone(long session_id, long tone,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerNode() throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerNodeResponse() throws TException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fetchProfileInfoRequest(long session_id,
			String calling_party_number, String called_party_number,
			String imsi_subscriber, String imei_subscriber,
			String redirection_number, boolean is_call,
			Map<String, String> extension) throws TException {
		// TODO Auto-generated method stub
		
	}

}
