package com.movirtu.mxcloudservice.action.ari;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.generated.Application;
import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class SubscribeChannelAction extends BaseAriAction{

	private Logger logger = Logger.getLogger(SubscribeChannelAction.class);
	public boolean perform(final Session session) throws Exception, RestException {
		// TODO Auto-generated method stub

        getAriConnector().getARI().applications().subscribe(getAriConnector().getApplicationName(), "channel:"+session.getChannelId() , new AriCallback<Application> () {
                public void onSuccess(Application result) {
                        // Let's do something with the returned value
                        logger.info("Application : "+ result + " subscribed successfully");
                    }

                    public void onFailure(RestException e) {
                        logger.warn( "Failure in subscribing to application ");
                        logger.warn(e);
                        e.printStackTrace();
                        onError(session);
                    }
        });

		return true;
	}

}
