package com.movirtu.mxcloudservice.connector.thrift.cap;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;

public class CapActionConsumer implements Consummable{

	mxcap.Client client;
	BaseThriftConnector connector;
	
	Logger logger = Logger.getLogger(CapActionConsumer.class);
	
	public CapActionConsumer(mxcap.Client client, BaseThriftConnector connector) {
		this.connector = connector;
		this.client = client;
		logger.info("CapActionConsumer created.");
	}

	private void onConnectorAction(ConnectorAction action) throws TException {
		//synchronized(client) {
		//synchronized(action.getSession().getSessionId()) {
		{
			if(action.getType()==ConnectorActionType.REGISTER_NODE) {
				logger.info("sending RegisterNodeRequest");
				client.registerNode();
			} else if (action.getType()== ConnectorActionType.REGISTER_NODE_RESPONSE) {
				logger.info("sending RegisterNodeResponse");
				client.registerNodeResponse();
			} else if(action.getType()==ConnectorActionType.CONNECTOR_REBUILD_CLIENT) {
				connector.recreateClient();
			}
		}
	}
	public void onEvent(Object obj) throws TException {
		logger.info("MapActionConsumer received an event :"+obj);
		if(obj instanceof ConnectorAction) {
			
			ConnectorAction action = (ConnectorAction) obj;
			logger.info("ConnectorAction received : connectorSessionId = "+action.getConnecterSessionId()
						+",sessionId = "+action.getSession().getSessionId()
						+",type = "+action.getType().toString()
						+",client = "+client);
			try {

				onConnectorAction(action);
			} catch(TException e) {
				//retry once
				logger.info(connector.getName()+" retrying connection.");
				connector.recreateClient();
				onConnectorAction(action);
			}

		}
	}

	public void setClient(mxcap.Client client) {
		// TODO Auto-generated method stub
		this.client = client;
	}
}
