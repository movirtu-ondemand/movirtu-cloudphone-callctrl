package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class ModifyServiceId extends CallBaseAction{

	private int serviceId;
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		session.setServiceId(getServiceId());
		return true;
	}
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

}
