package com.movirtu.mxcloudservice.service.machine;

public class StateMachineInTuple {
	private MachineEvent event;
	private MachineState state;
	/*
	public StateMachineInTuple(MachineEvent event, MachineState state) {
		this.event = event;
		this.state = state;
	}*/
	
	public MachineEvent getEvent() {
		return event;
	}
	public void setEvent(MachineEvent event) {
		this.event = event;
	}
	public MachineState getState() {
		return state;
	}
	public void setState(MachineState state) {
		this.state = state;
	}
	
	
}
