package com.movirtu.mxcloudservice;

import org.apache.log4j.Logger;

public class ApplicationState {
	public enum State {
		RUN(0), STOP(1), FORCE_STOP(2), UNDEFINED(4);
		private String name[] = {"RUN", "STOP", "FORCE_STOP", "UNDEFINED"};
		private int value;
	    private State(int value) {
	            this.value = value;
	    }
	    public int getValue(){
	        return value;
	    }
	    public String toString() {
	    	return name[value];
	    }
	}
	
	State state;
	Logger logger = Logger.getLogger(ApplicationState.class);
	
	private static ApplicationState applicationState = new ApplicationState();
	
	private ApplicationState() {
		state = State.UNDEFINED;
	}
	
	public boolean isRunning() {
		return state==State.RUN;
	}
	
	public static ApplicationState getInstance() { 
		return applicationState;
	}
	
	public State getApplicationState() {
		return state;
	}
	
	private void setState(State state) {
		applicationState.state = state;
	}
	
	public void changeState(State state) {
		applicationState.setState(state);
		logger.info("Application state changed to " + state);
	}
}
