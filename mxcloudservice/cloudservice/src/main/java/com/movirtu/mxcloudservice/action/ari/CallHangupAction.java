package com.movirtu.mxcloudservice.action.ari;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class CallHangupAction extends BaseAriAction {
	
	private String reason;
	private Logger logger = Logger.getLogger(CallHangupAction.class);
	
	public boolean perform(final Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		getAriConnector().getARI().channels().hangup(session.getChannelId(), 
													 getReason(), 
													 new AriCallback<Void>(){

														public void onFailure(
																RestException arg0) {
															// TODO Auto-generated method stub
															logger.error(arg0.toString());
															onError(session);
														}

														public void onSuccess(
																Void arg0) {
															// TODO Auto-generated method stub
															logger.info("Channel hangup request successful.");
														}
			
		});
		return true;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
