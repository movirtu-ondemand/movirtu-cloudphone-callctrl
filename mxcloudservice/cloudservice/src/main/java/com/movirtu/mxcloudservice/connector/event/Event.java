package com.movirtu.mxcloudservice.connector.event;

import java.util.HashMap;
import java.util.Map;

import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class Event {
	
	private String sessionId;
	private MachineEvent event;
	private Map<String,String> map;
	
	public Event(String str, MachineEvent event) {
		// TODO Auto-generated constructor stub
		this.sessionId = str;
		this.event = event;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public MachineEvent getEvent() {
		return event;
	}
	public void setEvent(MachineEvent event) {
		this.event = event;
	}
	public Map<String,String> getMap() {
		return map;
	}
	public void setMap(Map<String,String> map) {
		this.map = map;
	}
}
