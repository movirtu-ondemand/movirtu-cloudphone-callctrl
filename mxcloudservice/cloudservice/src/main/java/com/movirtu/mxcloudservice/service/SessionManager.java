package com.movirtu.mxcloudservice.service;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.movirtu.mxcloudservice.service.machine.MachineEvent;
import com.movirtu.mxcloudservice.service.machine.MachineState;
import com.movirtu.mxcloudservice.util.ExpirationQueue;
import com.movirtu.mxcloudservice.util.ExpireHandler;

public class SessionManager {
	private static ConcurrentHashMap map; 
	private static ExpirationQueue stateTimeoutQueue;
	private static SessionManager instance = new SessionManager();
	private Logger logger = Logger.getLogger(SessionManager.class);
	private SessionManager() {
		map = new ConcurrentHashMap();
		stateTimeoutQueue = new ExpirationQueue();
	}

	public static SessionManager getInstance() {
		return instance;
	}

	public synchronized BaseSession getSession(String sessionId) {
		if(map.containsKey(sessionId)) {
			logger.info("Session exists and being returned for sessionId = "+sessionId);
			return (BaseSession) map.get(sessionId);
		} else {
			/*Session session = new Session();
			session.setSessionId(sessionId);
			map.put(sessionId, session);
			logger.info("Session Object created with sessionId = "+sessionId);*/

			return null;
		}
	}

	public synchronized BaseSession getOrCreateSession(String sessionId, SessionFactory factory, Service service) {
		if(map.containsKey(sessionId)) {
			logger.info("Session exists and being returned for sessionId = "+sessionId);
			return (Session) map.get(sessionId);
		} else {
			BaseSession session = factory.createSession();
			session.setSessionId(sessionId);
			session.setService(service);
			session.setState(MachineState.NULL);
			map.put(sessionId, session);
			logger.info("Session Object created with sessionId = "+sessionId);
			return session;
		}
	}

	public synchronized void releaseSession(String sessionId) {
		map.remove(sessionId);
		logger.info("Session Object deleted with sessionId = "+sessionId);
	}

	public synchronized void releaseSessionsStartingWith(String name) {
		java.util.Iterator i = map.keySet().iterator();
		while(i.hasNext()) {
			String key = (String) i.next();
			if(key.startsWith(name)) {
				map.remove(key);
				logger.info("Session Object deleted with sessionId = "+key);
			}
		}
	}

	public synchronized void releaseSession(BaseSession session) {
		map.remove(session.getSessionId());
		logger.info("Session Object deleted with sessionId = "+session.getSessionId());
	}

	public void releaseAllSessions() {
		// TODO Auto-generated method stub
		map.clear();
		logger.info("All the session objects have been deleted");
	}

	public long size() {
		// TODO Auto-generated method stub
		return map.size();
	}
	
	public void putTimeout(String id, long timeout, MachineEvent event, ExpireHandler handler) {
		logger.info("Session state timeout created with id="+id+",timeout="+timeout+",event="+event);
		stateTimeoutQueue.putOrUpdateTimeout(id, timeout, event, handler);
	}
	
	public void removeTimeout(String id) {
		logger.info("Session state timeout removed for id="+id);
		stateTimeoutQueue.removeTimeout(id);
	}
}
