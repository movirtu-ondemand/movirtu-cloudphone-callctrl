package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.DiameterSession;

public class UpdateSubscriptionIdFromProfile extends BaseDiameterAction{

	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		session.getTransactionInfo().isVirtualNumber = session.getProfile().isVirtual();
		
		if(session.getProfile().isVirtual()) {
			session.getTransactionInfo().vSubscriptionIdTypeValue=session.isImsiBased()?1:0;
			session.getTransactionInfo().vSubscriptionIdDataValue=session.isImsiBased()?session.getProfile().getVirtualImsi():session.getProfile().getVirtualSubscriptionId();
			session.getNvp().put("subscriptionId", session.getTransactionInfo().vSubscriptionIdDataValue);
			session.getNvp().put("subscriptionIdType", ""+session.getTransactionInfo().vSubscriptionIdTypeValue);
		} else {
			session.getTransactionInfo().subscriptionIdTypeValue=session.isImsiBased()?1:0;		
			session.getTransactionInfo().subscriptionIdDataValue=session.isImsiBased()?session.getProfile().getVirtualImsi():session.getProfile().getVirtualSubscriptionId();
			session.getNvp().put("subscriptionId", session.getTransactionInfo().subscriptionIdDataValue);
			session.getNvp().put("subscriptionIdType", ""+session.getTransactionInfo().subscriptionIdTypeValue);
		}
		return true;
	}
}
