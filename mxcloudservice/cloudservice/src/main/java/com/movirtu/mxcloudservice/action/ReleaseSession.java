package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.connector.thrift.cdr.CDRClient;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.SessionManager;

public class ReleaseSession extends BaseAction{

	private boolean cdrEnabled=false;
	
	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		if(isCdrEnabled()) {
			session.getNvp().put("sessionId", session.getSessionId());
			CDRClient.sendCDR(session.getNvp());
		}
		SessionManager.getInstance().releaseSession(session);
		return true;
	}

	public boolean isCdrEnabled() {
		return cdrEnabled;
	}

	public void setCdrEnabled(boolean cdrEnabled) {
		this.cdrEnabled = cdrEnabled;
	}

}
