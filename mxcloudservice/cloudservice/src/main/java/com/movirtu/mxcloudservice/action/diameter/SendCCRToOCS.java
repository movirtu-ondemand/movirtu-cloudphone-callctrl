package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.connector.action.DiameterConnectorAction;
import com.movirtu.mxcloudservice.service.DiameterSession;

public class SendCCRToOCS extends BaseDiameterAction{

	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		//session.setTransactionInfo2(new TransactionInfo(session.getTransactionInfo1()));
		super.getConnector().getActionQueue().add(new DiameterConnectorAction(session, ConnectorActionType.SEND_CCR, session.getTransactionInfo()));

		return true;
	}

}

