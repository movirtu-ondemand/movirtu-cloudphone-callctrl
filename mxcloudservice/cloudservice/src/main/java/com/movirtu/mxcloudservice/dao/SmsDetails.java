package com.movirtu.mxcloudservice.dao;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="df_sms_info")
public class SmsDetails {

	//@Id
	//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="df_sms_info_id_seq")
	//	@SequenceGenerator(name="df_sms_info_id_seq", sequenceName="df_sms_info_id_seq", initialValue=1)
	//@Column(name = "id")

	/*@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="id_sequence")
	@SequenceGenerator(name="id_sequence",sequenceName="df_sms_info_id_seq", allocationSize=1)
	@Column(name="id", unique=true, nullable=false)*/
	@Id @GeneratedValue
	@Column(name="id")
	private int id;

	@Column(name="messageid")
	private String messageId;
	
	@Column(name="direction")
	private int direction;
	
	@Column(name="state")
	private int state;
	
	@Column(name="sender")
	private String sender;
	
	@Column(name="receiver")
	private String receiver;
	
	@Column(name="log_time")
	private Timestamp logTime;
	
	@Column(name="message")
	private String message;

	/*
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="id_sequence")
	@SequenceGenerator(name="id_sequence",sequenceName="df_sms_info_id_seq", allocationSize=1)
	@Column(name="id", unique=true, nullable=false)*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public Timestamp getLogTime() {
		return logTime;
	}
	public void setLogTime(Timestamp logTime) {
		this.logTime = logTime;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}


}
