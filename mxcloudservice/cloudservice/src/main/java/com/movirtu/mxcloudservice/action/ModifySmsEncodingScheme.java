package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class ModifySmsEncodingScheme extends CallBaseAction{

	private String encodingScheme;
	
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		if(session instanceof SmsSession) {
			((SmsSession) session).setEncodingScheme(getEncodingScheme());
		}
		return true;
	}

	public String getEncodingScheme() {
		return encodingScheme;
	}

	public void setEncodingScheme(String encodingScheme) {
		this.encodingScheme = encodingScheme;
	}


}
