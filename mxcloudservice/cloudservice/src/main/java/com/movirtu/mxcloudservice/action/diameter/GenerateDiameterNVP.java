package com.movirtu.mxcloudservice.action.diameter;

import java.util.Iterator;

import org.jdiameter.api.Avp;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.diameter.charging.apps.common.MultipleServiceCreditControlClass;
import com.movirtu.mxcloudservice.service.DiameterSession;

public class GenerateDiameterNVP extends BaseDiameterAction{

	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		if(session.getTransactionInfo()!=null) {
			
			putNVP(session, "apn", session.getTransactionInfo().calledStationId);
			putNVP(session,"ggsn_address", session.getTransactionInfo().ggsnAddress);
			putNVP(session,"ggsn_session", session.getTransactionInfo().key);
			putNVP(session,"sgsn_address", session.getTransactionInfo().sgsnAddress);
			putNVP(session,"charging_characteristic", session.getTransactionInfo().getChargingChar());
			putNVP(session,"ccfh", session.getTransactionInfo().creditControlFailureHandling);
			putNVP(session,"service_context_id", session.getTransactionInfo().getServiceContextId());
			putNVP(session,"charging_id", session.getTransactionInfo().getChargingId());
			putNVP(session,"context_type", session.getTransactionInfo().getContextType());
			putNVP(session,"imei", session.getTransactionInfo().getImeiSv());
			putNVP(session,"orig_host",session.getTransactionInfo().origHost);
			putNVP(session,"orig_state_id",session.getTransactionInfo().getOriginStateId());
			//putNVP(session,"orig_realm",session.getTransactionInfo().origRealm);
			putNVP(session,"gprs_qos_negotiated",session.getTransactionInfo().getGprsQosNeg());
			putNVP(session,"ggsn_mcc_mnc",session.getTransactionInfo().getGgsnMccMnc());
			putNVP(session,"framed_ip_address",session.getTransactionInfo().getFramedIpAddress());
			if(session.getTransactionInfo().msccList!=null) {
				for(Iterator<MultipleServiceCreditControlClass> it=session.getTransactionInfo().msccList.iterator();it.hasNext();) {
					MultipleServiceCreditControlClass obj = it.next();
					int index = obj.ratingGroup;
					putNVP(session,"mscc_rating_group_"+index,obj.ratingGroup);
					putNVP(session,"mscc_result_code_"+index,obj.resultCode);
					putNVP(session,"mscc_gsu_tarif_change_time_"+index,obj.gsuObj.tariifTimeChange);
					putNVP(session,"mscc_gsu_total_octets_"+index,obj.gsuObj.totalOctets);
					putNVP(session,"mscc_validity_time_"+index,obj.validityTime);
				}
			}
			
			//putNVP(session,"orig_host",session.getTransactionInfo().or);
			
		}
		return true;
	}

	private void putNVP(DiameterSession session, String key, Object value) {
		if(value!=null) {
			if(value instanceof Integer) {
				Integer i = (Integer) value;
				if(i.intValue()==-1)
					return;
			}
			session.getNvp().put(key, value.toString());
		}
	}
}
