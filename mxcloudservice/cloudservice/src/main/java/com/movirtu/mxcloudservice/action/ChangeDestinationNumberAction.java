package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public class ChangeDestinationNumberAction extends CallBaseAction{

	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		session.setCdpn(UserDetailsManager.getInstance().getSipUserFromPhoneNumber(session.getCdpn()));
		return true;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "ChangeDestinationNumberAction";
	}

}
