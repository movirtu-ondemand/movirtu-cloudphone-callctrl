package com.movirtu.mxcloudservice.action.thrift;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.movirtu.mxcloudservice.action.BaseAction;
import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public abstract class BaseThriftAction extends BaseAction{
	
	protected BaseThriftConnector connector;
	protected Logger logger = Logger.getLogger(BaseThriftAction.class);
	private String hashParameters;
	private HashMap<String,String> hashMap=new HashMap<String,String>();;
	
	public void setConnector(BaseThriftConnector connector) {
		this.connector = connector;
	}
	
	public BaseThriftConnector getConnector() {
		return connector;
	}
	
	protected boolean checkConstraints(BaseSession session) {
		if(!getConnector().isRunning()) {
			logger.error(getConnector().getName()+" is not running");
			session.getService().execute(session, MachineEvent.ERROR);
			return false;
		}
		return true;
	}
	
	public String getHashParameters() {
		return hashParameters;
	}
	
	public void setHashParameters(String hashParameters) {
		this.hashParameters = hashParameters;
		if(hashParameters!=null && !hashParameters.isEmpty()) {
			String[] list = hashParameters.split(";");
			for(int i=0;i<list.length;hashMap.put(list[i].split(":")[0], list[i].split(":")[1]),i++);
		}
	}
	
	public HashMap<String, String> getHashMap() {
		return hashMap;
	}
}
