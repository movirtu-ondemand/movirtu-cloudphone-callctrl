package com.movirtu.mxcloudservice.action;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public abstract class BaseAction implements Actionable{

	private String name;
	private boolean proceedEvenWithFailure=true;
	protected Logger logger = Logger.getLogger(BaseAction.class);
	
	public void setProceedEvenWithFailure(boolean proceedEvenWithFailure) {
		this.proceedEvenWithFailure = proceedEvenWithFailure;
	}
	
	public boolean getProceedEvenWithFailure() {
		return proceedEvenWithFailure;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void onError(BaseSession session) {
		session.getService().execute(session, MachineEvent.ERROR);
	}
	
	public boolean proceedEvenWithFailure() {
		return proceedEvenWithFailure;
	}
}
