package com.movirtu.mxcloudservice.util;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import com.movirtu.mxcloudservice.dao.SSIDRecord;
import com.movirtu.mxcloudservice.dao.manager.SSIDRecordDaoManager;

public class SSIDMap {
	
	private ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
	private static SSIDMap instance = null;
	
	
	//Lazy initialized
	public static SSIDMap getInstance() {
		if(instance == null){
			synchronized(SSIDMap.class){
				if(instance == null){
					instance = new SSIDMap();
				}
			}
		}
		return instance;
	}
	
	private SSIDMap() {
		ArrayList<SSIDRecord> list = SSIDRecordDaoManager.getInstance().getAllSSIDRecords();
		for(int i=0;i<list.size();i++) {
			SSIDRecord record = list.get(i);
			map.put(record.getSsid(), record.getCellId());
		}
	}
	
	public String getCellId(String ssid) {
		return map.get(ssid);
	}

}
