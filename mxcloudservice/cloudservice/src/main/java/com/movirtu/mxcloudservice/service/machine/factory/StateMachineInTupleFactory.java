package com.movirtu.mxcloudservice.service.machine.factory;

import org.apache.log4j.Logger;

import com.movirtu.mxcloudservice.service.machine.MachineEvent;
import com.movirtu.mxcloudservice.service.machine.MachineState;
import com.movirtu.mxcloudservice.service.machine.StateMachineInTuple;

public class StateMachineInTupleFactory {
	
	private StateMachineInTuple map[][];
	private static StateMachineInTupleFactory instance = new StateMachineInTupleFactory();
	private Logger logger = Logger.getLogger(StateMachineInTupleFactory.class);
	private StateMachineInTupleFactory() {
		int num_states = MachineState.values().length;
		int num_events = MachineEvent.values().length;
		
		MachineState state[] = MachineState.values();
		MachineEvent event[] = MachineEvent.values();
		
		map = new StateMachineInTuple[num_states][num_events];
		for(int i=0;i<num_states;i++)
			for(int j=0;j<num_events;j++) {
				map[i][j] = new StateMachineInTuple();
				map[i][j].setState(state[i]);
				map[i][j].setEvent(event[j]);
			}		
	}
	
	public static StateMachineInTupleFactory getInstance() {
		return instance;
	}
	
	public StateMachineInTuple getStateMachineInTuple(MachineState state, MachineEvent event) {
		logger.info("State="+state+",Event="+event);
		return map[state.getValue()][event.getValue()];
	}
}
