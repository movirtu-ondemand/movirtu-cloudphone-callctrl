package com.movirtu.mxcloudservice.connector.thrift.cdr;

import java.util.Map;

import com.movirtu.mxcloudservice.dao.CallDataRecord;

public interface AbstractCDRHandler {
	public CallDataRecord createCDR(Map<String,String> nvp);
}
