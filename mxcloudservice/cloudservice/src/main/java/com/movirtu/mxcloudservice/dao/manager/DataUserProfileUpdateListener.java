package com.movirtu.mxcloudservice.dao.manager;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PostUpdateEventListener;

import com.movirtu.mxcloudservice.dao.AbstractProfile;
import com.movirtu.mxcloudservice.dao.DataUserProfile;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.ProfileAble;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class DataUserProfileUpdateListener implements PostUpdateEventListener{

	ConcurrentHashMap<Integer, ProfileAble> map = new ConcurrentHashMap<Integer, ProfileAble>();
	Logger logger = Logger.getLogger(DataUserProfileUpdateListener.class);
	
	public void registerProfile(ProfileAble profileAble) {
		AbstractProfile profile = profileAble.getProfile();
		if(profile instanceof DataUserProfile) {
			DataUserProfile dataUserProfile = (DataUserProfile) profile;
			if(dataUserProfile!=null) {
				map.put(dataUserProfile.getId(), profileAble);
			}
		}
	}
	
	public void unregisterProfile(ProfileAble profileAble) {
		AbstractProfile profile = profileAble.getProfile();
		if(profile instanceof DataUserProfile) {
			DataUserProfile dataUserProfile = (DataUserProfile) profile;
			if(dataUserProfile!=null) {
				map.remove(dataUserProfile.getId());
			}
		}
	}
	
	public void onPostUpdate(PostUpdateEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getEntity() instanceof DataUserProfile) {
			DataUserProfile profile1 = (DataUserProfile) arg0.getEntity();
			logger.info("onPostUpdate profile="+profile1.toString());
			ProfileAble profileAble = map.get(profile1.getId());
			BaseSession session = (BaseSession) profileAble;
			if(profileAble!=null) {
				DataUserProfile profile2 = (DataUserProfile) profileAble.getProfile();
				if(profile2!=null) {
					if(profile2.equals(profile1)) {
						// profile has not changed
						session.getService().execute(session, MachineEvent.PROFILE_NOT_CHANGED);
					} else {
						// profile changed
						session.getService().execute(session, MachineEvent.PROFILE_CHANGED);
					}
				}
			}
		}
	}

}
