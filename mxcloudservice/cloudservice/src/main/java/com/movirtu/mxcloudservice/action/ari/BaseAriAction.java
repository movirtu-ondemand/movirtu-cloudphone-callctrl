package com.movirtu.mxcloudservice.action.ari;

import com.movirtu.mxcloudservice.action.Actionable;
import com.movirtu.mxcloudservice.action.BaseAction;
import com.movirtu.mxcloudservice.action.CallBaseAction;
import com.movirtu.mxcloudservice.connector.ari.AriConnector;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public abstract class BaseAriAction extends CallBaseAction {
	
	private AriConnector ariConnector;

	public AriConnector getAriConnector() {
		return ariConnector;
	}

	public void setAriConnector(AriConnector ariConnector) {
		this.ariConnector = ariConnector;
	}

}
