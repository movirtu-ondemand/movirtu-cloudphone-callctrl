package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.UserDetails;
import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.CallSession;
import com.movirtu.mxcloudservice.service.Session;

public class FetchProfileFromMsrn extends CallBaseAction{

	@Override
	public boolean perform(Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		
		if(session instanceof CallSession) {
			UserDetails ud = UserDetailsManager.getInstance().getUserDetailsFromMsrn(((CallSession)session).getMsrn());
			if(ud!=null) {
				session.setCdpn(ud.getPhoneNumber());
				session.setCalledPartySipUser(ud.getSipUser());
				session.setDestinationImsi(ud.getImsi());
			} else {
				if(proceedEvenWithFailure()) {
					return true;
				} else {
					onError(session);
				}
			}
		}
		return true;
	}

}
