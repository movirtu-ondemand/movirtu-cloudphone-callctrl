package com.movirtu.mxcloudservice.connector.thrift.cdr;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.TTransportFactory;

import com.movirtu.mxcloudservice.Application;
import com.movirtu.mxcloudservice.connector.GenericConsumer;
import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;
import com.movirtu.mxcloudservice.connector.thrift.map.MapEventConsumer;
import com.movirtu.mxcloudservice.connector.thrift.map.MapGatewayHandler;
import com.movirtu.mxcloudservice.connector.thrift.map.mapjavagw;

public class RemoteCDRConnector extends BaseThriftConnector{

	private HashMap<String, AbstractCDRHandler> handlerMap;
	private String handlerMapValue;
	
	private void initMap() {
		
		if(handlerMap!=null)
			return;
		
		handlerMap = new HashMap<String, AbstractCDRHandler>();
		
		if(handlerMapValue==null || handlerMapValue.isEmpty())
			handlerMapValue="default->NullHandler";
		
		String[] handlerList = handlerMapValue.split(",");
		for(int i=0;i<handlerList.length;i++) {
			String key=handlerList[i].split("->")[0];
			String value=handlerList[i].split("->")[1];
			AbstractCDRHandler handler = (AbstractCDRHandler)Application.getContext().getBean(value);
			handlerMap.put(key, handler);
		}
	}
	
	@Override
	protected void createClient() throws TTransportException {
		// TODO Auto-generated method stub
		initMap();
	}

	@Override
	protected void createServer() throws TTransportException,
			UnknownHostException, IOException {
		// TODO Auto-generated method stub
		TProtocolFactory protocolFactory = new TBinaryProtocol.Factory();
		TTransportFactory transportFactory = new TFramedTransport.Factory();
		serverTransport = new TServerSocket(getReceiverPort());
		
		CDRHandler handler = new CDRHandler(this);
		
		RemoteCDR.Processor processor = new RemoteCDR.Processor(handler); 
		
		server = new TSimpleServer(new Args(serverTransport).processor(processor).transportFactory(transportFactory).protocolFactory(protocolFactory));
		//
		//mapEventConsumer = new MapEventConsumer(getService(), this);
		eventConsumer = new GenericConsumer(inQueue, this, getEventExecutor() , new CDREventConsumer(this));
	}

	@Override
	public void recreateClient() throws TTransportException {
		// TODO Auto-generated method stub
		
	}

	public HashMap<String, AbstractCDRHandler> getHandlerMap() {
		return handlerMap;
	}

	public void setHandlerMap(HashMap<String, AbstractCDRHandler> handlerMap) {
		this.handlerMap = handlerMap;
	}

	public String getHandlerMapValue() {
		return handlerMapValue;
	}

	public void setHandlerMapValue(String handlerMapValue) {
		this.handlerMapValue = handlerMapValue;
	}

}
