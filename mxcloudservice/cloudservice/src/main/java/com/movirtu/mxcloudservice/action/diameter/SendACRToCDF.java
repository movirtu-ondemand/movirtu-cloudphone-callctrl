package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.connector.action.DiameterConnectorAction;
import com.movirtu.mxcloudservice.service.DiameterSession;

public class SendACRToCDF extends BaseDiameterAction{

	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		super.getConnector().getActionQueue().add(new DiameterConnectorAction(session, ConnectorActionType.SEND_ACR, session.getTransactionInfo()));

		return true;
	}

}
