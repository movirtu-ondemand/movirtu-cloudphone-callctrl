package com.movirtu.mxcloudservice.action.thrift;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public class ClearCacheEntry extends BaseThriftAction{

	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		getConnector().getCache().removeFromMaps(session.getSessionId());
		return true;
	}

}
