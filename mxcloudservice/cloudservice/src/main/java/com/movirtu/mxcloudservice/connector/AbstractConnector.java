package com.movirtu.mxcloudservice.connector;


public abstract class AbstractConnector implements Connector{
	protected State state = State.NULL;

	protected String name;
	protected Thread connectorThread = null;
	
	public void start() {
		connectorThread = new Thread(new Runnable() {

			public void run() {
				// TODO Auto-generated method stub
				changeState(State.STARTED);
				service();
				changeState(State.STOPPED);
			}
			
		});
		
		connectorThread.start();
	}
	
	public void enqueueInternalEvents(Object message) {
		//TODO PK
	}
	
	public void stop() {
		if(connectorThread!=null) {
			if(connectorThread.isAlive())
				connectorThread.interrupt();
		}
	}
	
	protected void changeState(State state) {
		this.state = state;
	}
	
	public boolean isRunning() {
		return (state == State.STARTED || state == State.RUNNING);		
	}

	public void onConnectorException(Exception e, Object obj) {
		// TODO Auto-generated method stub
		e.printStackTrace();
	}
	
}
