package com.movirtu.mxcloudservice.dao.manager;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.movirtu.mxcloudservice.Application;
import com.movirtu.mxcloudservice.dao.SmsDetails;

public class SmsDetailsManager {

	private static String config;
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;

	private Logger logger = Logger.getLogger(SmsDetailsManager.class);

	private static volatile SmsDetailsManager instance; //= new UserDetailsManager();

	private SmsDetailsManager(String config) {

		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			configuration.configure(Application.getXmlPath()+"/"+config);
			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}

	}

	public static void setDatabaseConfigFile(String sConfig) {
		config = sConfig;
	}

	public static SmsDetailsManager getInstance() {
		//		if(instance == null){
		//			synchronized(SmsDetailsManager.class){
		if(instance == null){
			instance = new SmsDetailsManager(config);
		}
		//			}
		//				}
		return instance;
	}


	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void closeSessionFactory() {
		if (sessionFactory != null)
			sessionFactory.close();
	}


	public void saveSms(String messageId,int direction,int state,String sender,String receiver,Timestamp logTime, String message) {
		System.out.println("Inside SmsDetailsManager saveSms");
		Session session = sessionFactory.openSession();
		Transaction tx=null;
		try {
			tx = session.beginTransaction();
			SmsDetails sms = new SmsDetails();
			sms.setMessageId(messageId);
			sms.setDirection(direction);
			sms.setState(state);
			sms.setSender(sender);
			sms.setReceiver(receiver);
			sms.setLogTime(logTime);
			sms.setMessage(message);
			session.save(sms);
			tx.commit();
		}
		catch (Exception ex) {
			if (tx!=null) {
				tx.rollback();
			}
			ex.printStackTrace();

			logger.error("Exception in saving SMS ", ex);
		}
		finally {
			session.close();
		}
	}

}
