package com.movirtu.mxcloudservice.action.sms.handler;

import com.movirtu.mxcloudservice.connector.Connector;
import com.movirtu.mxcloudservice.connector.ami.AmiMessage;
import com.movirtu.mxcloudservice.service.sms.SmsSession;

public class SmsSenderManager {

	private static volatile SmsSenderManager smsManager;
	private Connector connector;

	public Connector getConnector() {
		return connector;
	}

	public void setConnector(Connector connector) {
		this.connector = connector;
	}

	public static SmsSenderManager getInstance() {
		if(smsManager==null) {
			smsManager = new SmsSenderManager();
		}
		return smsManager;
	}

	private SmsSenderManager() {

	}

	public boolean sendSms(SmsSession sms) {
		connector.enqueueInternalEvents(new AmiMessage(sms.getCallingPartySipUser(), sms.getCalledPartySipUser(), sms.getMessageContent()));
		return true;
	}

}
