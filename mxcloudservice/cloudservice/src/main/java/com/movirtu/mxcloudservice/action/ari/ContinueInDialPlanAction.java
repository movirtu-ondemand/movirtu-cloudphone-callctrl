package com.movirtu.mxcloudservice.action.ari;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class ContinueInDialPlanAction extends BaseAriAction{

	private String dialPlan;
	private int priority;
	private Logger logger = Logger.getLogger(ContinueInDialPlanAction.class);
	
	public boolean perform(final Session session) throws Exception, RestException {
		// TODO Auto-generated method stub

        getAriConnector().getARI().channels().continueInDialplan(session.getChannelId(), 
        														 getDialPlan(),
        														 session.getCdpn(), 
        														 getPriority(), 
        														 new AriCallback<Void>() {
                        public void onSuccess(Void result) {
                        // Let's do something with the returned value
                        logger.info("continueInDialPlan successful");
                    }

                    public void onFailure(RestException e) {
                        logger.info( "continueInDialPlan failed "+e);
                        e.printStackTrace();
                        onError(session);
                    }
                });
        return true;
	}

	public String getDialPlan() {
		return dialPlan;
	}

	public void setDialPlan(String dialPlan) {
		this.dialPlan = dialPlan;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

}
