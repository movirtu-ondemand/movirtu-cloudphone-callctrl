package com.movirtu.mxcloudservice.connector;

public interface ConnectorStateListener {
	public void onConnectorStateChange(Connector connector, Connector.State fromState, Connector.State toState);
}
