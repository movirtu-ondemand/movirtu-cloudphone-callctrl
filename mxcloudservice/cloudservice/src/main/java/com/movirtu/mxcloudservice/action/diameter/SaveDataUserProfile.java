package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.DataUserProfile;
import com.movirtu.mxcloudservice.service.DiameterSession;

public class SaveDataUserProfile extends BaseDiameterAction{

	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		if(session.getFutureProfile()!=null)
			session.setProfile(session.getFutureProfile().get());
		else {
			//create dummy profile
			DataUserProfile profile=new DataUserProfile();
			if(session.isImsiBased()) {
				profile.setPhysicalSubscriptionId(session.getSubscriberId());
				profile.setVirtualSubscriptionId(session.getSubscriberId());
			} else {
				profile.setPhysicalImsi(session.getSubscriberId());
				profile.setVirtualImsi(session.getSessionId());
			}
			session.setProfile(profile);
		}
		return true;
	}

}
