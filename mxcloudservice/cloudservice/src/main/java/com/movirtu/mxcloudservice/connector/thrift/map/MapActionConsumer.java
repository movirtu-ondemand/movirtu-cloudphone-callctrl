package com.movirtu.mxcloudservice.connector.thrift.map;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;
import com.movirtu.mxcloudservice.connector.thrift.Performer;
import com.movirtu.mxcloudservice.connector.thrift.map.mapjavainterface.Client;
import com.movirtu.mxcloudservice.connector.thrift.map.perform.AbstractMapPerformer;
import com.movirtu.mxcloudservice.connector.thrift.map.perform.RegisterNodeRequest;
import com.movirtu.mxcloudservice.connector.thrift.map.perform.RegisterNodeResponse;
import com.movirtu.mxcloudservice.connector.thrift.map.perform.ReqOutgoingSms;
import com.movirtu.mxcloudservice.connector.thrift.map.perform.RespIncomingSmsFailure;
import com.movirtu.mxcloudservice.connector.thrift.map.perform.RespIncomingSmsSuccess;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class MapActionConsumer implements Consummable{

	mapjavainterface.Client client;
	BaseThriftConnector connector;
	HashMap<ConnectorActionType, Performer> actionMap = new HashMap<ConnectorActionType, Performer>();
	
	Logger logger = Logger.getLogger(MapActionConsumer.class);
	
	public MapActionConsumer(mapjavainterface.Client client, BaseThriftConnector connector) {
		this.connector = connector;
		this.client = client;
		buildActionMap();
		logger.info("MapActionConsumer created.");
	}

	private void buildActionMap() {
		actionMap.put(ConnectorActionType.REGISTER_NODE, new RegisterNodeRequest(client));
		actionMap.put(ConnectorActionType.REGISTER_NODE_RESPONSE, new RegisterNodeResponse(client));
		actionMap.put(ConnectorActionType.INCOMING_SMS_RESPONSE_FAILURE, new RespIncomingSmsFailure(client));
		actionMap.put(ConnectorActionType.INCOMING_SMS_RESPONSE_SUCCESS, new RespIncomingSmsSuccess(client));
		actionMap.put(ConnectorActionType.SEND_SMS, new ReqOutgoingSms(client));
	}
	
	private void onConnectorAction(ConnectorAction action) throws TException {
		 if(action.getType()==ConnectorActionType.CONNECTOR_REBUILD_CLIENT) {
			connector.recreateClient();
		}
	}
	
	private void pushErrorEvent(BaseSession session) {
		session.getService().execute(session, MachineEvent.ERROR);
	}
	
	public void onEvent(Object obj) throws TException {
		logger.info("MapActionConsumer received an event :"+obj);
		if(obj instanceof ConnectorAction) {
			
			ConnectorAction action = (ConnectorAction) obj;
			
			synchronized(action.getSession().getSessionId()) {
			
				logger.info("ConnectorAction received : connectorSessionId = "+action.getConnecterSessionId()
						+",sessionId = "+action.getSession().getSessionId()
						+",type = "+action.getType().toString()
						+",client = "+client);
				
				try {
					Performer performer = actionMap.get(action.getType());
					if(performer!=null) 
						performer.perform(action);
					else
						onConnectorAction(action);
				} catch(TException e) {
					//retry once
					logger.info(connector.getName()+" retrying connection.");
					try {
						connector.recreateClient();
						onConnectorAction(action);
					} catch(TException e1) {
						logger.error("failure to recreate the transport "+e1.getMessage());
						pushErrorEvent(action.getSession());
					}
				}
			}
		
		}
	}

	public void setClient(Client client) {
		// TODO Auto-generated method stub
		this.client = client;
		for(java.util.Iterator<Performer> i = actionMap.values().iterator();i.hasNext();) {
			((AbstractMapPerformer)i.next()).setClient(client);;
		}
	}
}
