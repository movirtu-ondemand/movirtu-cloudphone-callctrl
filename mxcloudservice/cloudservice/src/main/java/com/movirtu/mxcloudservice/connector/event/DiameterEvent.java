package com.movirtu.mxcloudservice.connector.event;

import org.jdiameter.api.Message;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class DiameterEvent extends Event{

	private TransactionInfo info;
	private Message message;
	
	public DiameterEvent(String str, MachineEvent event, TransactionInfo info) {
		super(str, event);
		this.setInfo(info);
		// TODO Auto-generated constructor stub
	}

	public DiameterEvent(String str, MachineEvent event, Message message) {
		super(str, event);
		this.setInfo(null);
		this.setMessage(message);
		// TODO Auto-generated constructor stub
	}
	
	public DiameterEvent(String str, MachineEvent event) {
		super(str, event);
		this.setInfo(null);
		this.setMessage(null);
		// TODO Auto-generated constructor stub
	}
	
	public TransactionInfo getInfo() {
		return info;
	}

	public void setInfo(TransactionInfo info) {
		this.info = info;
	}
	
	public String toString() {
		if(getInfo()!=null)
			return "DiameterEvent:type="+getEvent()+",key="+getInfo().key+",apn="+getInfo().calledStationId;
		else
			return "DiameterEvent:type="+getEvent()+",sessionId="+getSessionId();
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
}
