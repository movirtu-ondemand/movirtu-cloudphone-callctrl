package com.movirtu.mxcloudservice.dao.manager;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.movirtu.mxcloudservice.Application;
import com.movirtu.mxcloudservice.dao.CallDataRecord;

public class CallDataRecordManager extends BaseDaoManager{
	
	private static String config;
	private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;
    
    private Logger logger = Logger.getLogger(UserDetailsManager.class);
    
    private static CallDataRecordManager instance; //= new UserDetailsManager();
    
    private CallDataRecordManager(String config) {

            try {
                    // Create the SessionFactory from hibernate.cfg.xml
                    Configuration configuration = new Configuration();
                    configuration.configure(Application.getXmlPath()+"/"+config);
                    serviceRegistry = new ServiceRegistryBuilder().applySettings(
                                    configuration.getProperties()).buildServiceRegistry();
                    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Throwable ex) {
                    // Make sure you log the exception, as it might be swallowed
                    System.err.println("Initial SessionFactory creation failed." + ex);
                    throw new ExceptionInInitializerError(ex);
            }
  
    }
    
    public static void setDatabaseConfigFile(String sConfig) {
    	config = sConfig;
    }
    
    public static CallDataRecordManager getInstance() {
    	if(instance == null){
            synchronized(CallDataRecordManager.class){
                if(instance == null){
                       instance = new CallDataRecordManager(config);
                }
            }
        }
    	return instance;
    }
    
    
    public static SessionFactory getSessionFactory() {
            return sessionFactory;
    }

    public static void closeSessionFactory() {
    if (sessionFactory != null)
        sessionFactory.close();
    }
    
    public void writeCallDataRecordAsync(final CallDataRecord cdr) {
    	executor.submit(new Callable<Void>(){
			public Void call() {
				writeCallDataRecord(cdr);
				return null;
			}
		});
    }
    
    public void writeCallDataRecord(CallDataRecord cdr) {
    	Session session = sessionFactory.openSession();
    	session.beginTransaction();
    	session.save(cdr);
    	//session.get(UserDetails.class, arg1)
    	session.getTransaction().commit();
    	session.close();
    	logger.info("CDR Writing Successful "+cdr);
    }
}
