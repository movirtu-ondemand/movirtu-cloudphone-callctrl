package com.movirtu.mxcloudservice.action;

import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public abstract class BaseAsyncAction extends BaseAction{

	private MachineEvent successEvent;
	private MachineEvent failureEvent;
	
	public MachineEvent getSuccessEvent() {
		return successEvent;
	}

	public void setSuccessEvent(MachineEvent successEvent) {
		this.successEvent = successEvent;
	}

	public MachineEvent getFailureEvent() {
		return failureEvent;
	}

	public void setFailureEvent(MachineEvent failureEvent) {
		this.failureEvent = failureEvent;
	}

}
