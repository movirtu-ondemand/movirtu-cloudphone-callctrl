package com.movirtu.mxcloudservice.connector.thrift;

import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.action.ConnectorAction;

public interface Performer {
	public void perform(ConnectorAction action) throws TException; 
}
