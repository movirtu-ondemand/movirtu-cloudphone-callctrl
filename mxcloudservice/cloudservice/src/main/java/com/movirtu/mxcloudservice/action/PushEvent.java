package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class PushEvent extends BaseAction{

	MachineEvent event;
	
	public void setEvent(MachineEvent event) {
		this.event = event;
	}
	
	public MachineEvent getEvent() {
		return event;
	}
	
	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		session.getService().execute(session, event);
		return true;
	}

}
