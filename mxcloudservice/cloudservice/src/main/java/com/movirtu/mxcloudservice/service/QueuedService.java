package com.movirtu.mxcloudservice.service;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class QueuedService extends GenericService implements Runnable {

	private BlockingQueue<QueueObject> queue=null;
	private int queueSize;
	private Thread currentThread=null;
	
	public QueuedService(){
		super();
	}
	
	@Override
	public void init() {
		super.init();
		if(currentThread==null) {
			currentThread = (new Thread(this));
			currentThread.start();
		}
	}
	
	@Override
	public void execute(BaseSession session, MachineEvent event) {
		if(queue!=null) {
			try {
				queue.put(new QueueObject(session,event));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else 
			super.execute(session,event);
	}


	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
		queue = new ArrayBlockingQueue<QueueObject>(queueSize);
	}
	
	public static class EndCommand {
		
	}
	
	public static class QueueObject {
		private BaseSession session;
		private MachineEvent event;
		
		public QueueObject(BaseSession session, MachineEvent event) {
			this.session = session;
			this.event = event;
		}
		
		public BaseSession getSession() {
			return session;
		}
		public void setSession(Session session) {
			this.session = session;
		}
		public MachineEvent getEvent() {
			return event;
		}
		public void setEvent(MachineEvent event) {
			this.event = event;
		}
	}

	public void run() {
		// TODO Auto-generated method stub
		Object obj=null;
		try {
			while(true) {
				obj = queue.poll(300, TimeUnit.SECONDS);
				if(obj!=null) {
					if(obj instanceof EndCommand) {
						throw new Exception("Queue has been terminated.");
					} else if(obj instanceof QueueObject) {
						QueueObject queueObject = (QueueObject) obj;
						super.execute(queueObject.getSession(), queueObject.getEvent());
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//e.printStackTrace();
		} catch (Exception e) {
			//connector.onConnectorException(e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
