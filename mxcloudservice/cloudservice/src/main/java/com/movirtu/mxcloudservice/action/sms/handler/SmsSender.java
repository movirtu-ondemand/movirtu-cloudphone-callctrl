package com.movirtu.mxcloudservice.action.sms.handler;

import com.movirtu.mxcloudservice.service.sms.SmsSession;


public interface SmsSender {

	public boolean sendSms(SmsSession sms);

}
