package com.movirtu.mxcloudservice.action.diameter;


import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.DataUserProfile;
import com.movirtu.mxcloudservice.service.DiameterSession;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class TestProfileSwitch extends BaseDiameterAction{

	private MachineEvent okEvent;
	private MachineEvent nokEvent;
	
	Logger logger = Logger.getLogger(TestProfileSwitch.class);
	
	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		if(session.getFutureProfile()!=null) {
			DataUserProfile profile1 = session.getFutureProfile().get();
			DataUserProfile profile2 = session.getProfile();
			if(profile1!=null && profile2!=null) {
				/*if(profile1.getActive()==profile2.getActive()
						&& profile1.getPhysicalSubscriptionId()== profile2.getPhysicalSubscriptionId()
						&& profile1.getVirtualSubscriptionId()== profile2.getVirtualSubscriptionId())*/
				if(profile1.equals(profile2)){
					session.getService().execute(session, okEvent);
				} else {
					logger.error("profile1 and profile2 did not match profile1="+profile1+",profile2="+profile2);
					session.getService().execute(session, nokEvent);
				}
				
			} else {
				logger.error("Newly fetched profile or existing profile is null");
				session.getService().execute(session, nokEvent);
			}	
			//session.setProfile(session.getFutureProfile().get());
		} else {
			logger.error("FutureProfile object is null");
			session.getService().execute(session, nokEvent);
		}
		return true;
	}

	public MachineEvent getOkEvent() {
		return okEvent;
	}

	public void setOkEvent(MachineEvent okEvent) {
		this.okEvent = okEvent;
	}

	public MachineEvent getNokEvent() {
		return nokEvent;
	}

	public void setNokEvent(MachineEvent nokEvent) {
		this.nokEvent = nokEvent;
	}

}
