package com.movirtu.mxcloudservice.connector.diameter.ro.ctf;

import org.apache.thrift.TException;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwCCRBuilder;
import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.connector.action.DiameterConnectorAction;
import com.movirtu.mxcloudservice.service.DiameterSession;

public class CTFConnectorActionConsumer implements Consummable{

	
	private CTFConnector connector;

	public CTFConnectorActionConsumer(CTFConnector ctfConnector) {
		// TODO Auto-generated constructor stub
		this.connector = ctfConnector;
	}

	public void onEvent(Object obj) throws TException {
		// TODO Auto-generated method stub
		if(obj instanceof DiameterConnectorAction) {
			DiameterConnectorAction action = (DiameterConnectorAction) obj;
			if(action.getType()==ConnectorActionType.SEND_CCA) {
			
			int type = action.getTransactionInfo().recordType;
			action.getTransactionInfo().key = action.getSession().getSessionId();
			action.getTransactionInfo().request = ((DiameterSession)action.getSession()).getCcrRequest();
			action.getTransactionInfo().serverCCASession = ((DiameterSession)action.getSession()).getServerCCASession();
			try {
					if(type==B2BGwCCRBuilder.CC_REQUEST_TYPE_INITIAL) {
						connector.getClient().sendInitial(action.getTransactionInfo());
					} else if(type==B2BGwCCRBuilder.CC_REQUEST_TYPE_INTERIM) {
						connector.getClient().sendInterim(action.getTransactionInfo());
					} else if(type==B2BGwCCRBuilder.CC_REQUEST_TYPE_TERMINATE) {
						connector.getClient().sendTerminate(action.getTransactionInfo());
					}
				} catch(Exception e) {
					e.printStackTrace();
					connector.onConnectorException(e, obj);
				}
			}
		}
	}

}
