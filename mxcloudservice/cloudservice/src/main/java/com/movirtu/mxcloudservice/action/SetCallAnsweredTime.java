package com.movirtu.mxcloudservice.action;

import java.util.Date;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.CallSession;
import com.movirtu.mxcloudservice.service.Session;

public class SetCallAnsweredTime extends CallBaseAction{

        @Override
        public boolean perform(Session session) throws Exception, RestException {
                // TODO Auto-generated method stub
                if(session instanceof CallSession) {
                        CallSession callSession = (CallSession) session;
                        callSession.setCallAnsweredTime(new Date());
                        return true;
                } else {
                        return super.proceedEvenWithFailure();
                }
        }
}