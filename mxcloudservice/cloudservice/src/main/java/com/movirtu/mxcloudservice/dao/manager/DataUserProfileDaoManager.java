package com.movirtu.mxcloudservice.dao.manager;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PostUpdateEventListener;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.movirtu.mxcloudservice.Application;
import com.movirtu.mxcloudservice.dao.DataUserProfile;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class DataUserProfileDaoManager extends BaseDaoManager{

	private static String config;
	private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;
    
    private Logger logger = Logger.getLogger(DataUserProfileDaoManager.class);
    
    private static DataUserProfileDaoManager instance; //= new UserDetailsManager();
    
    private DataUserProfileDaoManager(String config) {

        try {
                // Create the SessionFactory from hibernate.cfg.xml
                Configuration configuration = new Configuration();
                PostUpdateEventListener[] stack = {new DataUserProfileUpdateListener()};
                //configuration.configure(Application.getXmlPath()+"/"+config);
                configuration.configure(new File(Application.getXmlPath()+"/"+config));
                
                //configuration.add
                //configuration.addResource("/"+Application.getXmlPath()+"/"+"DataUserProfile.hbm.xml");
                serviceRegistry = new ServiceRegistryBuilder().applySettings(
                                configuration.getProperties()).buildServiceRegistry();

                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                //EventListenerRegistry registry = ((SessionFactoryImpl) sessionFactory).getServiceRegistry().getService(EventListenerRegistry.class);
                //registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(new DataUserProfileUpdateListener());

        } catch (Throwable ex) {
                // Make sure you log the exception, as it might be swallowed
                System.err.println("Initial SessionFactory creation failed." + ex);
                throw new ExceptionInInitializerError(ex);
        }

    }
    
    public static void setDatabaseConfigFile(String sConfig) {
    	config = sConfig;
    }
    
    public static DataUserProfileDaoManager getInstance() {
    	if(instance == null){
            synchronized(DataUserProfileDaoManager.class){
                if(instance == null){
                       instance = new DataUserProfileDaoManager(config);
                }
            }
        }
    	return instance;
    }
    
    
    public static SessionFactory getSessionFactory() {
            return sessionFactory;
    }

    public static void closeSessionFactory() {
    if (sessionFactory != null)
        sessionFactory.close();
    }
 
	public class FetchDataUserProfile implements Callable<DataUserProfile>{

		String[] columnName;
		Object[] value;
		com.movirtu.mxcloudservice.service.BaseSession session;
		MachineEvent successEvent;
		MachineEvent failureEvent;
		boolean virtualProfile;
		boolean useDefault;
		boolean imsiBased;
		
		public FetchDataUserProfile(String[] columnName, Object[] value, com.movirtu.mxcloudservice.service.BaseSession session, MachineEvent successEvent, MachineEvent failureEvent) {
			this.columnName = columnName;
			this.value = value;
			this.session = session;
			this.successEvent = successEvent;
			this.failureEvent = failureEvent;
			this.useDefault = true;
		}
		public FetchDataUserProfile(String[] columnName2, Object[] value2,
				BaseSession session2, boolean virtualProfile, boolean imsiBased,
				MachineEvent successEvent2, MachineEvent failureEvent2) {
			this.columnName = columnName2;
			this.value = value2;
			this.session = session2;
			this.successEvent = successEvent2;
			this.failureEvent = failureEvent2;
			this.virtualProfile = virtualProfile;
			this.useDefault = false;
			this.imsiBased = imsiBased;
		}
		//@Override
		public DataUserProfile call() throws Exception {
			// TODO Auto-generated method stub
			DataUserProfile ret = null;
			if(useDefault)
				ret = getProfile(columnName, value);
			else {
				if(virtualProfile && imsiBased) {
					ret = getProfile(columnName, value, false);
					if(ret!=null) {
						columnName[0] = "physicalSubscriptionId";
						value[0] = ret.getPhysicalSubscriptionId();
						ret = getProfile(columnName, value, true);
					} else {
						session.getService().execute(session, failureEvent);
						return null;
					}
				} else {
					ret = getProfile(columnName, value, virtualProfile);
				}
			}

			if(ret!=null) {
				session.getService().execute(session, successEvent);
				return ret;
			} else {
				session.getService().execute(session, failureEvent);
				return null;
			}
		}
	}
    
    public Future<DataUserProfile> getProfileAsync(String[] columnName, Object[] value, com.movirtu.mxcloudservice.service.BaseSession session, MachineEvent successEvent, MachineEvent failureEvent) {
		return executor.submit(new FetchDataUserProfile(columnName,value,session, successEvent, failureEvent));
    }
    
    public Future<DataUserProfile> getProfileAsync(String[] columnName, Object[] value, boolean virtualProfile, boolean imsiBased, com.movirtu.mxcloudservice.service.BaseSession session, MachineEvent successEvent, MachineEvent failureEvent) {
		return executor.submit(new FetchDataUserProfile(columnName,value,session, virtualProfile,imsiBased, successEvent, failureEvent));
    }
    
    public DataUserProfile getProfile(String[] columnName, Object[] value) {
		DataUserProfile ret = null;
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			Criteria cr = session.createCriteria(DataUserProfile.class);
			if(columnName!=null && value!=null) {
				for(int i=0;i<columnName.length;i++) {
					cr.add(Restrictions.eq(columnName[i], value[i]));
					logger.info("getProfile critera: columnName="+columnName[i]+",value="+value[i]);
				}
			}
			
			List<DataUserProfile> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
			if(list != null && list.size()>0) {
				DataUserProfile ud = (DataUserProfile) list.get(0);
				ret = ud;
				logger.info("DataUserProfile found = "+ ud);
			} else 
				logger.info("DataUserProfile not found");
			session.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error reading database "+e.getMessage());
		} finally {
			session.close();
		}
		return ret;
    }
    
    public DataUserProfile getProfile(String[] columnName, Object[] value, boolean virtualProfile) {
		DataUserProfile ret = null;
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			Criteria cr = session.createCriteria(DataUserProfile.class);
			if(virtualProfile)
				cr.add(Restrictions.neProperty("physicalSubscriptionId", "virtualSubscriptionId"));
			else
				cr.add(Restrictions.eqProperty("physicalSubscriptionId", "virtualSubscriptionId"));

			if(columnName!=null && value!=null) {
				for(int i=0;i<columnName.length;i++) {
					cr.add(Restrictions.eq(columnName[i], value[i]));
					logger.info("getProfile critera: columnName="+columnName[i]+",value="+value[i]);
				}
			}
			
			List<DataUserProfile> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
			if(list != null && list.size()>0) {
				DataUserProfile ud = (DataUserProfile) list.get(0);
				ret = ud;
				logger.info("DataUserProfile found = "+ ud);
			} else 
				logger.info("DataUserProfile not found");
			session.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error reading database "+e.getMessage());
		} finally {
			session.close();
		}
		return ret;    	
    }
    
    public void testConnection() {
    	Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(DataUserProfile.class);
		try {
			
			List<DataUserProfile> list = cr.list();//session.createQuery("from UserDetails where sipUser="+sipUser).list();
			if(list != null && list.size()>0) {
				DataUserProfile ud = (DataUserProfile) list.get(0);
				logger.info("DataUserProfile found = "+ ud);
			} else 
				logger.info("DataUserProfile not found");
			session.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error reading database "+e.getMessage());
		} finally {
			session.close();
		}
    }
    
}
