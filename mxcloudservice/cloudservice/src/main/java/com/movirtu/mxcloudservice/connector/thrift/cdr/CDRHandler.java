package com.movirtu.mxcloudservice.connector.thrift.cdr;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.event.CDREvent;
import com.movirtu.mxcloudservice.connector.thrift.BaseThriftConnector;

public class CDRHandler implements RemoteCDR.Iface{

	BaseThriftConnector connector;
	Logger logger = Logger.getLogger(CDRHandler.class);
	
	public CDRHandler(BaseThriftConnector connector) {
		this.connector = connector;
	}
	
	@Override
	public void sendCDR(Map<String, String> nvp) throws TException {
		// TODO Auto-generated method stub
		connector.getEventQueue().add(new CDREvent(nvp));
	}

}
