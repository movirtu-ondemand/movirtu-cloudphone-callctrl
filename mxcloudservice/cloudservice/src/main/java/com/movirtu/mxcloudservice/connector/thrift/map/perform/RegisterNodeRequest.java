package com.movirtu.mxcloudservice.connector.thrift.map.perform;

import java.util.HashMap;

import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.thrift.map.mapjavainterface.Client;

public class RegisterNodeRequest extends AbstractMapPerformer{

	public RegisterNodeRequest(Client client) {
		super(client);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void perform(ConnectorAction action) throws TException {
		// TODO Auto-generated method stub
		logger.info("sending RegisterNodeRequest");
	    try {
			client.registerNodeRequest(action.getMap());	
	    }catch(TException e) {
	    	logger.error("failed to send RegisterNode "+e.getMessage());
	    }
	}
}
