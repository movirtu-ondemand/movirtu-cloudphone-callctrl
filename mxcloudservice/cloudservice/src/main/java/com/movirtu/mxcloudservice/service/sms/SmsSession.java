package com.movirtu.mxcloudservice.service.sms;

import java.sql.Timestamp;

import com.movirtu.mxcloudservice.service.Session;

public class SmsSession extends Session {

	private String messageContent;
	private String messageId;
	private String logTime;
	private Timestamp sqlTimestamp;
	private String encodingScheme;

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}

	public Timestamp getSqlTimestamp() {
		return sqlTimestamp;
	}

	public void setSqlTimestamp(Timestamp sqlTimestamp) {
		this.sqlTimestamp = sqlTimestamp;
	}

	public String getEncodingScheme() {
		return encodingScheme;
	}

	public void setEncodingScheme(String encodingScheme) {
		this.encodingScheme = encodingScheme;
	}
}
