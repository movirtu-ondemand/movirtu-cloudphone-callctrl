package com.movirtu.mxcloudservice.action.diameter;

import com.movirtu.mxcloudservice.service.DiameterSession;

public class FetchDataUserProfileApn extends FetchDataUserProfileAsync{

	private String[] column1 = {"apn","physicalSubscriptionId"};
	private String[] column2 = {"apn","imsi"};
	private Object[] value= {"",""};
	
	@Override
	protected String[] getColumns(DiameterSession session) {
		// TODO Auto-generated method stub
		return session.isImsiBased()?column2:column1;
	}

	@Override
	protected Object[] getValues(DiameterSession session) {
		// TODO Auto-generated method stub
		value[0] = session.getApn();
		value[1] = session.getSubscriberId();
		return value;
	}

}
