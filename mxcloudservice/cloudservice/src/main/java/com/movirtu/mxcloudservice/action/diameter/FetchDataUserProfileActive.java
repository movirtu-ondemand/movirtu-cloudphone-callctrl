package com.movirtu.mxcloudservice.action.diameter;

import com.movirtu.mxcloudservice.service.DiameterSession;

public class FetchDataUserProfileActive extends FetchDataUserProfileAsync{

	private String[] column1 = {"active","physicalSubscriptionId"};
	private String[] column2 = {"active","imsi"};
	private Object[] value= {"",""};
	
	@Override
	protected String[] getColumns(DiameterSession session) {
		// TODO Auto-generated method stub
		return session.isImsiBased()?column2:column1;
	}

	@Override
	protected Object[] getValues(DiameterSession session) {
		// TODO Auto-generated method stub
		value[0] = Boolean.TRUE;
		value[1] = session.getSubscriberId();
		return value;
	}

}
