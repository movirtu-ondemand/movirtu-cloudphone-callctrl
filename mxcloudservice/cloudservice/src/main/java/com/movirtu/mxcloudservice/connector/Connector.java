package com.movirtu.mxcloudservice.connector;

public interface Connector {

	public enum State {
		STARTED(0), RUNNING(1), STOPPED(2), NULL(3);
		private int value;
		private String name[] = {"STARTED","RUNNING","STOPPED","NULL"};
	    private State(int value) {
	            this.value = value;
	    }
	    public int getValue(){
	        return value;
	    }
	    public String toString() {
	    	return name[value];
	    }
	}

	
	public void start();
	public void service();
	public void stop();
	public State getState();
	public void ping();
	public void enqueueInternalEvents(Object message);
	public boolean isRunning();
	public String getName();
	public void onConnectorException(Exception e, Object obj);
}
