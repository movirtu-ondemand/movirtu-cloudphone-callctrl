package com.movirtu.mxcloudservice.service;

import java.util.Date;

public class CallSession extends Session{
	private String msrn;
	private Date callAttemptedTime=null;
	private Date callAnsweredTime=null;
	private Date callHangupTime=null;
	
	public String getMsrn() {
		return msrn;
	}

	public void setMsrn(String msrn) {
		this.msrn = msrn;
	}

	public Date getCallAttemptedTime() {
		return callAttemptedTime;
	}

	public void setCallAttemptedTime(Date callAttemptedTime) {
		this.callAttemptedTime = callAttemptedTime;
	}

	public Date getCallAnsweredTime() {
		return callAnsweredTime;
	}

	public void setCallAnsweredTime(Date callAnsweredTime) {
		this.callAnsweredTime = callAnsweredTime;
	}

	public Date getCallHangupTime() {
		return callHangupTime;
	}

	public void setCallHangupTime(Date callHangupTime) {
		this.callHangupTime = callHangupTime;
	}
	
}
