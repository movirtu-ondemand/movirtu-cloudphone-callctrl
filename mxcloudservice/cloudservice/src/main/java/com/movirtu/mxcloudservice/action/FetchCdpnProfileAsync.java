package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.Session;

public class FetchCdpnProfileAsync extends CallBaseAsyncAction{

	private String columnName;
	
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	public String getColumnName() {
		return columnName;
	}
	
	@Override
	public boolean perform(Session session) throws Exception, RestException {
		if(columnName==null) {
			logger.error("columnName is null.");
			return proceedEvenWithFailure();
		}
		session.setCdpnProfileFuture(UserDetailsManager.getInstance().fetchUserDetailsAsync(columnName, 
				  session.getCdpn(), 
				  session, 
				  getSuccessEvent(), 
				  getFailureEvent()));
		return true;
	}

}
