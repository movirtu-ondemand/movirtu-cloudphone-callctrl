package com.movirtu.mxcloudservice.connector.thrift.map.perform;

import org.apache.log4j.Logger;

import com.movirtu.mxcloudservice.connector.thrift.Performer;
import com.movirtu.mxcloudservice.connector.thrift.map.mapjavainterface;

public abstract class AbstractMapPerformer implements Performer{
	
	protected mapjavainterface.Client client;

	Logger logger = Logger.getLogger(AbstractMapPerformer.class);
	
	public AbstractMapPerformer(mapjavainterface.Client client) {
		this.client = client;
	}
	
	public mapjavainterface.Client getClient() {
		return client;
	}

	public void setClient(mapjavainterface.Client client) {
		this.client = client;
	}
	
}
