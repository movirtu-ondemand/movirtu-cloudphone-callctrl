package com.movirtu.mxcloudservice.connector.diameter.rf.ctf;

import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwSendFromServiceInvocationWrapperForAnswer;
import com.movirtu.diameter.charging.apps.B2BGw.server.B2BGwServerAcc;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.diameter.test.b2bgw.ServiceProcessingDiameterMessages;
import com.movirtu.mxcloudservice.connector.GenericConsumer;
import com.movirtu.mxcloudservice.connector.diameter.rf.OfflineChargingConnector;
import com.movirtu.mxcloudservice.connector.event.ACADiameterEvent;
import com.movirtu.mxcloudservice.connector.event.ACRDiameterEvent;

public class CTFConnector extends OfflineChargingConnector implements ServiceProcessingDiameterMessages{

	B2BGwServerAcc server;
	B2BGwSendFromServiceInvocationWrapperForAnswer client;

	protected void createClient() {
		// TODO Auto-generated method stub
		client = B2BGwSendFromServiceInvocationWrapperForAnswer.INSTANCE;
		actionConsumer = new GenericConsumer(outQueue, this, getActionExecutor(),new CTFConnectorActionConsumer());
	}


	protected void createServer() throws Exception {
		// TODO Auto-generated method stub
		eventConsumer = new GenericConsumer(inQueue, this, getEventExecutor() , new CTFConnectorEventConsumer());
		server = new B2BGwServerAcc(this);
		server.init(getClass().getResourceAsStream(getConfigFileName()),"SERVER1");
		server.start();
	}


	public void serviceProcessingACA(TransactionInfo obj) {
		// TODO Auto-generated method stub
		super.getEventQueue().add(new ACADiameterEvent(obj));
	}


	public void serviceProcessingACR(TransactionInfo obj) {
		// TODO Auto-generated method stub
		super.getEventQueue().add(new ACRDiameterEvent(obj));
	}


	public void replyOfACRWrapperToService(TransactionInfo obj) {
		// TODO Auto-generated method stub
		// do nothing
	}

}
