package com.movirtu.mxcloudservice.connector.ami;

import java.io.IOException;

import org.asteriskjava.manager.TimeoutException;
import org.asteriskjava.manager.action.MessageSendAction;


public class SmsAmiConnector extends AmiConnector {

	@Override
	public String generateSessionId(String id) {
		return getName()+id;
	}

	@Override
	public void onInternalEvent(AmiMessage m) {
		sendSms(m);
	}

	@Override
	public void onEvent(AmiMessage m) {
		// TODO Auto-generated method stub

	}

	public boolean sendSms(AmiMessage message) {
		System.out.println("Sending sms using AMI "+message);

		//TODO save sms to DB
		MessageSendAction msgSnd = new MessageSendAction();
		String to = message.getCalledParty();
		if(to==null) {
			System.out.println("TO field can not be null. Returning without sending SMS");
			// TODO send an event SMS_FAILED;
			return false;
		}

		msgSnd.setTo("pjsip:"+to);
		String from = message.getCallingParty();
		msgSnd.setFrom(from);
		msgSnd.setBody(message.getMessageContent());

		try {
			System.out.println("Sending sms "+msgSnd);
			dmc.sendAction(msgSnd);
			return true;
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void onConnectorException(Exception e, Object obj) {
		// TODO Auto-generated method stub
		
	}

}
