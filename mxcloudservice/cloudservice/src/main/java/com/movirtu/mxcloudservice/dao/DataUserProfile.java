package com.movirtu.mxcloudservice.dao;

import java.util.Date;

public class DataUserProfile extends AbstractProfile{
	
	private int id;
	private boolean active;
	private String apn;
	private String virtualSubscriptionId;
	private String physicalSubscriptionId;
	private String virtualImsi;
	private String physicalImsi;
	private Date lastChanged;
	private char type;
	// type='a' -> APN based
	// type='p' -> profile based
	public String toString() {
		return "id=["+id+
				"],active=["+active+
				"],apn=["+apn+
				"],virtualSubscriptionId=["+virtualSubscriptionId+
				"],physicalSubscriptionId=["+physicalSubscriptionId+
				"],virtualImsi=["+virtualImsi+
				"],physicalImsi=["+physicalImsi+
				"],lastChanged=["+lastChanged+
				"]";
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean isActive() {
		return active;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getApn() {
		return apn;
	}
	public void setApn(String apn) {
		this.apn = apn;
	}
	public String getVirtualSubscriptionId() {
		return virtualSubscriptionId;
	}
	public void setVirtualSubscriptionId(String virtualSubscriptionId) {
		this.virtualSubscriptionId = virtualSubscriptionId;
	}
	public String getPhysicalSubscriptionId() {
		return physicalSubscriptionId;
	}
	public void setPhysicalSubscriptionId(String physicalSubscriptionId) {
		this.physicalSubscriptionId = physicalSubscriptionId;
	}
	public Date getLastChanged() {
		return lastChanged;
	}
	public void setLastChanged(Date lastChanged) {
		this.lastChanged = lastChanged;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public String getVirtualImsi() {
		return virtualImsi;
	}

	public void setVirtualImsi(String virtualImsi) {
		this.virtualImsi = virtualImsi;
	}

	public String getPhysicalImsi() {
		return physicalImsi;
	}

	public void setPhysicalImsi(String physicalImsi) {
		this.physicalImsi = physicalImsi;
	}
	
	public boolean isVirtualSubscriptionId() {
		return this.virtualSubscriptionId!=this.physicalSubscriptionId;
	}
	
	public boolean isVirtualImsi() {
		return this.virtualImsi!=this.physicalImsi;
	}
	
	public boolean isVirtual() {
		return isVirtualSubscriptionId();
	}
	
	@Override public boolean equals(Object obj) { 
		if (obj == this) { 
			return true; 
		} 
		if (obj == null || obj.getClass() != this.getClass()) { 
			return false; 
		}
		
		DataUserProfile profile = (DataUserProfile) obj;
		/*if(profile.getId()==getId())
			return true;
		else
			return false;*/
		return getId()==profile.getId() &&
			   getActive()==profile.getActive() &&
			   (getApn() == profile.getApn() || (getApn()!=null && getApn().equals(profile.getApn()))) &&
			   (getPhysicalImsi() == profile.getPhysicalImsi() || (getPhysicalImsi()!=null && getPhysicalImsi().equals(profile.getPhysicalImsi()))) &&
			   (getPhysicalSubscriptionId() == profile.getPhysicalSubscriptionId() || (getPhysicalSubscriptionId()!=null &&(getPhysicalSubscriptionId().equals(profile.getPhysicalSubscriptionId())))) &&
			   (getType() == profile.getType()) &&
			   (getVirtualImsi()==profile.getVirtualImsi() || (getVirtualImsi()!=null && (getVirtualImsi().equals(profile.getVirtualImsi())))) &&
			   (getVirtualSubscriptionId()==profile.getVirtualSubscriptionId() || (getVirtualSubscriptionId()!=null && (getVirtualSubscriptionId().equals(profile.getVirtualSubscriptionId()))));
	}
		//Person guest = (Person) obj; return id == guest.id && (firstName == guest.firstName || (firstName != null && firstName.equals(guest.getFirstName()))) && (lastName == guest.lastName || (lastName != null && lastName .equals(guest.getLastName()))); }

}
