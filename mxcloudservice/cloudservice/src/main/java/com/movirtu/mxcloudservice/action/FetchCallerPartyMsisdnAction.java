package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.manager.UserDetailsManager;
import com.movirtu.mxcloudservice.service.Session;

public class FetchCallerPartyMsisdnAction extends CallBaseAction {

	public boolean perform(Session session) throws Exception, RestException {
		System.out.println("Inside "+getName());
		
		String callerParty = session.getCgpn();

		if(callerParty == null) {
			return false;
		}

		if(UserDetailsManager.getInstance().isSipUser(callerParty)) {
			session.setCgpn(UserDetailsManager.getInstance().getPhoneNumberFromSipUser(callerParty));
		}

		return true;
	}

	public String getName() {
		return "FetchCallerPartySipUserAction";
	}

}
