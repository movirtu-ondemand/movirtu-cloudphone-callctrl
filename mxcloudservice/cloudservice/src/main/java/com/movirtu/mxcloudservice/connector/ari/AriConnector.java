package com.movirtu.mxcloudservice.connector.ari;


import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.ARI;
import ch.loway.oss.ari4java.AriVersion;
import ch.loway.oss.ari4java.generated.AsteriskInfo;
import ch.loway.oss.ari4java.generated.Message;
import ch.loway.oss.ari4java.tools.ARIException;
import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.MessageQueue;
import ch.loway.oss.ari4java.tools.MessageQueue.ErrorMessage;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.connector.AbstractConnector;
import com.movirtu.mxcloudservice.service.Service;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.SessionFactory;
import com.movirtu.mxcloudservice.service.SessionManager;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public abstract class AriConnector extends AbstractConnector {

	private String ipAddress;
	private int port;
	private String applicationName;
	private String userName;
	private String password;
	private int version;
	private int wsMaxPolling;
	private int wsMaxInterval;
	private MessageQueue messageQueue;
	private SessionFactory sessionFactory;
	//private String prefix="";
	protected Service service;
	
	private ARI ari = null;
	protected Logger logger = Logger.getLogger(AriConnector.class);
	//private UserDetailsManager udm = new UserDetailsManager();
	
	/*public UserDetailsManager getUserDetailsManager() {
		return udm;
	}*/
	
	public abstract String generateSessionId(String id);
	

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	public AriVersion getAriVersion() {
		if(version==0)
			return AriVersion.ARI_0_0_1;
		else if (version==1)
			return AriVersion.ARI_1_0_0;
		else
			return AriVersion.IM_FEELING_LUCKY;
	}

	public State getState() {
		// TODO Auto-generated method stub
		return state;
	}
	
	public ARI getARI() {
		return ari;
	}
	
	public void connect() throws ARIException {
		ari = ARI.build("http://"+getIpAddress()+":"+getPort()+"/",
						getApplicationName(),
						getUserName(), 
						getPassword(), 
						getAriVersion());
		
		AsteriskInfo ai = ari.asterisk().getInfo("");
		logger.info(getName()+" Connection established with Asterisk");
		//logger.info(ai.getSystem());
		// log out
	}
	
	public void service() {
		// TODO Auto-generated method stub
		try {
			connect();
			
			changeState(State.RUNNING);
			
			onInit();
			
			processEvents();
			
			changeState(State.STOPPED);
		} catch(ARIException e) {
			logger.error(getName()+" "+e.toString());
			//e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(getName()+" "+e.toString());
			//e.printStackTrace();
			
		} finally {
			onDestroy();
			
		}
	}
	
	protected void onInit() {
		Session session = (Session) SessionManager.getInstance().getOrCreateSession(getName(),sessionFactory,service);
		getService().init();
		getService().execute(session, MachineEvent.INIT);
	}
	
	protected void onDestroy() {
		Session session = (Session) SessionManager.getInstance().getOrCreateSession(getName(),sessionFactory,service);
		getService().execute(session, MachineEvent.DESTROY);
	}
	
	public void processEvents() throws ARIException, InterruptedException {
		
		MessageQueue mq = ari.getWebsocketQueue();
		
		this.messageQueue = mq;
		
		long start = System.currentTimeMillis();
        
		while(isRunning()) {
        
            Message m = mq.dequeueMax( getWsMaxPolling(), getWsMaxInterval() );
            if (m != null) {

                long now = System.currentTimeMillis() - start;
                logger.info(now + ": " + getName()+": "+ m);
                if(m instanceof ErrorMessage) {
                	logger.error(((ErrorMessage) m).getType());
                	throw new ARIException("asterisk disconnected");
                } else {
                	onEvent(m);
                }
            }
        }
	}
	
	public abstract void onEvent(Message m);

	public int getWsMaxPolling() {
		return wsMaxPolling;
	}

	public void setWsMaxPolling(int wsMaxPolling) {
		this.wsMaxPolling = wsMaxPolling;
	}

	public int getWsMaxInterval() {
		return wsMaxInterval;
	}

	public void setWsMaxInterval(int wsMaxInterval) {
		this.wsMaxInterval = wsMaxInterval;
	}
/*
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
*/
	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public synchronized void ping() {
		// TODO Auto-generated method stub
		ari.asterisk().getInfo("", new AriCallback<AsteriskInfo>(){

			public void onFailure(RestException arg0) {
				// TODO Auto-generated method stub
				logger.info(arg0.toString());
				changeState(State.STOPPED);
				try {
				messageQueue.notify();
				} catch(Exception e) {
					logger.info(e.toString());
				}
				
			}

			public void onSuccess(AsteriskInfo arg0) {
				// TODO Auto-generated method stu
				logger.info("Asterisk connected details for connector="+getName());
				logger.info("Name:"+arg0.getConfig().getName());
				logger.info("Startup Time:"+arg0.getStatus().getStartup_time()
						   +",Reload Time:"+arg0.getStatus().getLast_reload_time());
				logger.info("Entity Id:"+arg0.getSystem().getEntity_id()
						   +",Version:"+arg0.getSystem().getVersion());
				logger.info("Default language:"+arg0.getConfig().getDefault_language()
						   +",Max Channels:"+arg0.getConfig().getMax_channels()
						   +",Max Load:"+arg0.getConfig().getMax_channels()
						   +",Max Open files:"+arg0.getConfig().getMax_open_files()
						   );
				logger.info("Build date:"+arg0.getBuild().getDate()
						+",Kernel:"+arg0.getBuild().getKernel()
						+",Machine:"+arg0.getBuild().getMachine()
						+",OS:"+arg0.getBuild().getOs());
			}
			
		});
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
