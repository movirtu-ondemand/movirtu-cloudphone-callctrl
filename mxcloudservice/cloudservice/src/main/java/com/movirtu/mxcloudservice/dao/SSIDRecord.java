package com.movirtu.mxcloudservice.dao;

public class SSIDRecord {
	private String ssid;
	private String cellId;
	private int id;
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public String getCellId() {
		return cellId;
	}
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	
	public String toString() {
		return "SSIDRecord:ssid="+ssid+",cellId="+cellId+",id="+id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
