package com.movirtu.mxcloudservice.connector.event;

import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class SmsEvent extends Event{

	private int serviceId;
	private String imsi;
	private String codingScheme;
	private String dataString;
	private String origPartyAddress;
	private String timeStamp;
	private long mapSessionId;
	
	public SmsEvent(String str, long session_id, MachineEvent event, int service_id,
			String imsi, String coding_scheme, String data_string,
			String orig_party_addr, String timestamp) {
		super(str, event);
		this.setMapSessionId(session_id);
		this.setServiceId(service_id);
		this.setImsi(imsi);
		this.setCodingScheme(coding_scheme);
		this.setDataString(data_string);
		this.setOrigPartyAddress(orig_party_addr);
		this.setTimeStamp(timestamp);
		// TODO Auto-generated constructor stub
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getCodingScheme() {
		return codingScheme;
	}

	public void setCodingScheme(String codingScheme) {
		this.codingScheme = codingScheme;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}

	public String getOrigPartyAddress() {
		return origPartyAddress;
	}

	public void setOrigPartyAddress(String origPartyAddress) {
		this.origPartyAddress = origPartyAddress;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public long getMapSessionId() {
		// TODO Auto-generated method stub
		return mapSessionId;
	}

	public void setMapSessionId(long mapSessionId) {
		this.mapSessionId = mapSessionId;
	}
	
	
}
