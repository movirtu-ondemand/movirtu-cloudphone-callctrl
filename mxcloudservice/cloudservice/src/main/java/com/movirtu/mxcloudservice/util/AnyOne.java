package com.movirtu.mxcloudservice.util;

import java.util.HashMap;

import org.apache.log4j.Logger;

public class AnyOne {
	
	private long bitMap;
	private long upValue;
	private HashMap<String, Integer> anyOneMap = new HashMap<String, Integer>();
	Logger logger = Logger.getLogger(AnyOne.class);
	
	public AnyOne(String values[]) throws Exception {
		if(values.length > 32)
			throw new Exception("Number of dependent connectors cannot be more than 32");
		upValue=0;
		for(int i=0;i<values.length;i++) {
			anyOneMap.put(values[i], i);
			upValue = upValue | (1 << i);
		}
		bitMap = 0;
	}
	
	public void setUp(String str) {
		if(anyOneMap!=null && anyOneMap.containsKey(str)) {
			bitMap = bitMap | (1 << (int) anyOneMap.get(str));
		//	logger.info("AnyOne bitMap = "+bitMap+", upValue="+upValue);
		}
	}
	
	public void setDown(String str) {
		if(anyOneMap!=null && anyOneMap.containsKey(str)) {
			bitMap = bitMap & (0 << (int) anyOneMap.get(str));
		//	logger.info("AnyOne bitMap = "+bitMap+", upValue="+upValue);
		}
	}
	
	public boolean isActive() {
		if(bitMap == upValue)
			return true;
		else
			return false;
	}
}
