package com.movirtu.mxcloudservice.connector.diameter.ro.ocs;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.event.DiameterEvent;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.DiameterSession;
import com.movirtu.mxcloudservice.service.SessionManager;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class OCSConnectorEventConsumer implements Consummable{

	
	private OCSConnector connector;
	private Logger logger = Logger.getLogger(OCSConnectorEventConsumer.class);

	public OCSConnectorEventConsumer(OCSConnector ocsConnector) {
		// TODO Auto-generated constructor stub
		this.connector = ocsConnector;
	}

	private DiameterSession getDiameterSession(String sessionId1) {
		if(sessionId1==null)
			return null;
		
		String sessionId = connector.getCache().getSession1From2(sessionId1);
		
		if(sessionId==null) {
			logger.error("There is no session corresponding to sessionId="+sessionId1);
			return null;
		}
		
		DiameterSession session = (DiameterSession) SessionManager.getInstance().getSession(sessionId);
		
		return session;
	}
	
	public void onEvent(Object obj) throws TException {
		// TODO Auto-generated method stub
		if(obj instanceof DiameterEvent) {
			DiameterEvent event = (DiameterEvent) obj;
			if(event.getEvent()==MachineEvent.CCA) {
				String sessionId = connector.getCache().getSession1From2(event.getInfo().key);
				if(sessionId!=null) {
					DiameterSession session = (DiameterSession) SessionManager.getInstance().getSession(sessionId);
					
					session.setTransactionInfo(event.getInfo());
					connector.getService().execute(session, event.getEvent());
				} else {
					logger.error("There is no session corresponding to key="+event.getInfo().key);
				}
			} else if(event.getEvent()==MachineEvent.ALLOW_FAILOVER_DELIVERY_FAILURE) {
				DiameterSession session = getDiameterSession(event.getSessionId());
				if(session!=null)
					connector.getService().execute(session, event.getEvent());
			} else if(event.getEvent()==MachineEvent.DENY_FAILOVER_DELIVERY_FAILURE) {
				DiameterSession session = getDiameterSession(event.getSessionId());
				if(session!=null)
					connector.getService().execute(session, event.getEvent());
			} else if(event.getEvent()==MachineEvent.ALLOW_FAILOVER_FAILURE_MESSAGE) {
				DiameterSession session = getDiameterSession(event.getSessionId());
				if(session!=null)
					connector.getService().execute(session, event.getEvent());
			} else if(event.getEvent()==MachineEvent.DENY_FAILOVER_FAILURE_MESSAGE) {
				DiameterSession session = getDiameterSession(event.getSessionId());
				if(session!=null)
					connector.getService().execute(session, event.getEvent());
			} else if(event.getEvent()==MachineEvent.ALLOW_FAILOVER_TIMEOUT) {
				DiameterSession session = getDiameterSession(event.getSessionId());
				if(session!=null)
					connector.getService().execute(session, event.getEvent());
			} else if(event.getEvent()==MachineEvent.DENY_FAILOVER_TIMEOUT) {
				DiameterSession session = getDiameterSession(event.getSessionId());
				if(session!=null)
					connector.getService().execute(session, event.getEvent());
			}
		}
	}
}

