package com.movirtu.mxcloudservice.service.machine;

import java.util.concurrent.ConcurrentHashMap;

public enum MachineState {
	NULL(0), 
	STARTED(1), 
	CONNECTING(2), 
	CONNECTED(3), 
	DISCONNECTING(4),
	DISCONNECTED(5),
	UNDEFINED(6),

	// SMS specific states
	SMS_PENDING(7), 
	SMS_SENT(8),
	SMS_FAILED(9),
	SMS_DELIVERED(10),
	ACTIVE(11),
	WAIT_FOR_PROFILE(12),
	WAIT_FOR_PROFILE2(13),
	WAIT_FOR_SERVICE_TYPE(14),
	CONNECTED_ACK(15)
	;

	private int value;
	private static String name[] = {
			"NULL", 
			"STARTED", 
			"CONNECTING", 
			"CONNECTED", 
			"DISCONNECTING",
			"DISCONNECTED",
			"UNDEFINED",

			// SMS specific states
			"SMS_PENDING", 
			"SMS_SENT",
			"SMS_FAILED",
			"SMS_DELIVERED",
			"ACTIVE",
			"WAIT_FOR_PROFILE",
			"WAIT_FOR_PROFILE2",
			"WAIT_FOR_SERVICE_TYPE",
			"CONNECTED_ACK"
	};

	private MachineState(int value) {
		this.value = value;
	}

	public int getValue(){
		return value;
	}

	public String toString() {
		return name[value];
	}

	public static MachineState fromString(String text) {
	    if (text != null) {
	      for (MachineState b : MachineState.values()) {
	        if (text.equalsIgnoreCase(name[b.value])) {
	          return b;
	        }
	      }
	    }
	    System.out.println("MachineState :"+text+" does not exist");
	    return null;
	  }
}
