package com.movirtu.mxcloudservice.service;

import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public interface Service {
	public void init();
	public void setThreadPoolSize(int threadPoolSize);
	public void execute(BaseSession session, MachineEvent event);
	public boolean isActive();
}
