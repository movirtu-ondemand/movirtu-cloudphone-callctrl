package com.movirtu.mxcloudservice.action.thrift.map;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.thrift.BaseThriftAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.action.ConnectorActionType;
import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public class RebuildClient extends BaseThriftAction{

	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		ConnectorAction action = new ConnectorAction(ConnectorActionType.CONNECTOR_REBUILD_CLIENT);
		action.setSession(session);
		action.setConnecterSessionId(0);
		//action.setConnecterSessionId(getConnector().getCache().getConnectorSessionIdFromServiceSessionId(session.getSessionId()));
		getConnector().getActionQueue().add(action);
		return true;
	}

}
