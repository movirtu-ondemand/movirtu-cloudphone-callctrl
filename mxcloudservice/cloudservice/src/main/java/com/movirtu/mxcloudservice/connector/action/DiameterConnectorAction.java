package com.movirtu.mxcloudservice.connector.action;

import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.mxcloudservice.service.BaseSession;

public class DiameterConnectorAction extends ConnectorAction{

	private TransactionInfo transactionInfo;
	
	public DiameterConnectorAction(BaseSession session, ConnectorActionType type, TransactionInfo transactionInfo) {
		super(type);
		super.setSession(session);
		this.transactionInfo = transactionInfo;
		// TODO Auto-generated constructor stub
	}

	public void setTransactionInfo(TransactionInfo transactionInfo) {
		this.transactionInfo = transactionInfo;
	}
	
	public TransactionInfo getTransactionInfo() {
		return this.transactionInfo;
	}
	
	public String toString() {
		return "DiameterConnectorAction:type="+getType()+",key="
				+transactionInfo.key+",apn="+transactionInfo.calledStationId
				+",subscriberId="+transactionInfo.subscriptionIdDataValue;
	}
}
