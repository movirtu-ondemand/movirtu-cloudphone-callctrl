package com.movirtu.mxcloudservice.connector.thrift.cdr;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.print.DocFlavor.STRING;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.Consummable;
import com.movirtu.mxcloudservice.connector.event.CDREvent;
import com.movirtu.mxcloudservice.dao.CallDataRecord;
import com.movirtu.mxcloudservice.dao.manager.CallDataRecordManager;

public class CDREventConsumer implements Consummable{

	Logger logger = Logger.getLogger(CDREventConsumer.class);
	private RemoteCDRConnector connector;
	
	public CDREventConsumer(RemoteCDRConnector connector) {
		this.connector = connector;
	}
	
	CallDataRecord createCDRFromMap(Map<String, String> nvp) {
		CallDataRecord cdr=null;
		String src=nvp.get("source");
		
		if(src==null || src.isEmpty())
			src="default";
		
		AbstractCDRHandler handler = connector.getHandlerMap().get(src);
		
		if(handler!=null)
			handler.createCDR(nvp);
		
	/*	
		if(nvp.get("source").compareToIgnoreCase("capgateway")==0)
			cdr = CapCDRHandler.createCDR(nvp);
	*/
		return cdr;
		//
	}
	
	private void dumpNVP(Map<String, String> nvp) {
		// TODO Auto-generated method stub
		logger.info("################ NVP Received ##################");
		Iterator it = nvp.entrySet().iterator();
		while(it.hasNext()) {
			Entry<String,String> entry = (Entry<String, String>) it.next();
			logger.info(entry.getKey()+"="+entry.getValue());
		}
	}

	public void onEvent(Object obj) throws TException {
		// TODO Auto-generated method stub
		if(obj instanceof CDREvent) {
			CDREvent cdrEvent = (CDREvent) obj;
			dumpNVP(cdrEvent.getMap());
			
			CallDataRecord cdr = createCDRFromMap(cdrEvent.getMap());
			
			if(cdr!=null) {
				//logger.info("CDR prepared = "+cdr.toString());
				CallDataRecordManager.getInstance().writeCallDataRecord(cdr);
			}
				
			//logger.info(serializeMap(cdr.getMap()));
		}
	}

}
