package com.movirtu.mxcloudservice.connector.action;

public enum ConnectorActionType {

	//MAP Connector Actions
	REGISTER_NODE(0),
	SEND_SMS(1), 
	REGISTER_NODE_RESPONSE(2),
	INCOMING_SMS_RESPONSE_SUCCESS(3), 
	INCOMING_SMS_RESPONSE_FAILURE(4),
	CONNECTOR_REBUILD_CLIENT(5),

	//Diameter Connector Actions
	SEND_ACR(6),
	SEND_ACA(7),
	SEND_CCR(8),
	SEND_CCA(9)
	;

	private int value;
	private String name[] = {
			"REGISTER_NODE(0)",
			"SEND_SMS(1)",
			"REGISTER_NODE_RESPONSE(2)",
			"INCOMING_SMS_RESPONSE_SUCCESS(3)", 
			"INCOMING_SMS_RESPONSE_FAILURE(4)",
			"CONNECTOR_REBUILD_CLIENT(5)",
			"SEND_ACR(6)",
			"SEND_ACA(7)",
			"SEND_CCR(8)",
			"SEND_CCA(9)"
	};

	private ConnectorActionType(int value) {
		this.value = value;
	}

	public int getValue(){
		return value;
	}

	public String toString() {
		return name[value];
	}
}
