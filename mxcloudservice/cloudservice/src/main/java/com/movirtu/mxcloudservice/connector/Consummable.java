package com.movirtu.mxcloudservice.connector;

import org.apache.thrift.TException;

public interface Consummable {
	void onEvent(Object obj) throws TException;
}
