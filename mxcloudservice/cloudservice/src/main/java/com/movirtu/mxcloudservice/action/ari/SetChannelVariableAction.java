package com.movirtu.mxcloudservice.action.ari;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.Session;

public class SetChannelVariableAction extends BaseAriAction{

	private String variableName;
	private String variableValue;
	
	Logger logger = Logger.getLogger(SetChannelVariableAction.class);
	
	public boolean perform(final Session session) throws Exception, RestException {
		// TODO Auto-generated method stub
		logger.info("channel="+session.getChannelId()+",set "+getVariableName()+"="+getVariableValue());
		getAriConnector().getARI().channels().setChannelVar(session.getChannelId(), 
				getVariableName(), getVariableValue(), 
				new AriCallback<Void>() {
						
						public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						
						}
						
						public void onFailure(RestException e) {
						// TODO Auto-generated method stub
						e.printStackTrace();
						logger.warn(e);
						onError(session);
						}

			});

		return true;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getVariableValue() {
		return variableValue;
	}

	public void setVariableValue(String variableValue) {
		this.variableValue = variableValue;
	}

}
