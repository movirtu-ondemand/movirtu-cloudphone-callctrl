package com.movirtu.mxcloudservice.service;

import com.movirtu.mxcloudservice.dao.AbstractProfile;

public interface ProfileAble {
	public AbstractProfile getProfile();
}
