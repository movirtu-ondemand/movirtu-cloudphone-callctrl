package com.movirtu.mxcloudservice.connector.thrift.map.perform;

import java.util.HashMap;

import org.apache.thrift.TException;

import com.movirtu.mxcloudservice.connector.action.ConnectorAction;
import com.movirtu.mxcloudservice.connector.thrift.map.mapjavainterface.Client;
import com.movirtu.mxcloudservice.service.Session;

public class RespIncomingSmsSuccess extends AbstractMapPerformer{

	public RespIncomingSmsSuccess(Client client) {
		super(client);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void perform(ConnectorAction action) throws TException {
		// TODO Auto-generated method stub
		int serviceId = ((Session) (action.getSession())).getServiceId();
		logger.info("sending respIncommingSmsData for success");
		client.respIncommingSmsData(serviceId, 
									action.getConnecterSessionId(), 
									(short)-1, new HashMap<String, String>());
	}

}
