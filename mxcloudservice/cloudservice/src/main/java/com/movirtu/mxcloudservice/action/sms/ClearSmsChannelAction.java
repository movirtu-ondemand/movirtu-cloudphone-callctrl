package com.movirtu.mxcloudservice.action.sms;

import org.apache.log4j.Logger;

import ch.loway.oss.ari4java.tools.AriCallback;
import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.action.ari.BaseAriAction;
import com.movirtu.mxcloudservice.service.Session;

public class ClearSmsChannelAction extends BaseAriAction {

	private Logger logger = Logger.getLogger(ParseAndCreateOutgoingSmsAction.class);

	public boolean perform(Session session) throws Exception, RestException {
		System.out.println("ClearSmsChannelAction : Hello sms state machine");

		getAriConnector().getARI().channels().continueInDialplan(session.getChannelId(), 
				session.getContext(),
				session.getCdpn(), 
				(int) session.getPriority()+1, 
				new AriCallback<Void>() {
			public void onSuccess(Void result) {
				// Let's do something with the returned value
				logger.info("continueAction successful");
			}

			public void onFailure(RestException e) {
				logger.info( "continueAction failed "+e);
				e.printStackTrace();

			}
		});
		return true;
	}

}
