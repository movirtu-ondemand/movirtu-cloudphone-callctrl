package com.movirtu.mxcloudservice.action;

import java.sql.Timestamp;
import java.util.Date;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.dao.CallDataRecord;
import com.movirtu.mxcloudservice.dao.UserDetails;
import com.movirtu.mxcloudservice.dao.manager.CallDataRecordManager;
import com.movirtu.mxcloudservice.service.CallSession;
import com.movirtu.mxcloudservice.service.Session;
import com.movirtu.mxcloudservice.service.sms.SmsSession;
import com.movirtu.mxcloudservice.util.Counter;
import com.movirtu.mxcloudservice.util.SSIDMap;

public class WriteCDRAction extends CallBaseAction{

	private int transactionType;
	private String basicService;
	private int subscriberNumberLength;
	private int otherPartyNumberLength;
	private static Counter counter = new Counter(6);
	//MTC-2
	//MOC-1
	//MTC-SMS-30
	//MOC-SMS-31
	
	public boolean perform(Session session) throws Exception, RestException {
		
		CallDataRecord cdr=null;
		
		switch(transactionType) {
			case 1: 
			case 2:
					cdr = forCall((CallSession) session);
					break;
			case 10:
			case 11: 
					cdr = forSms((SmsSession) session);
					break;
		}
		
		//temp
		
		
		cdr.setSequenceNumber(counter.getNext());
		cdr.setBasicService(basicService);
		CallDataRecordManager.getInstance().writeCallDataRecord(cdr);
		return true;
	}
	
	private CallDataRecord forCall(CallSession session) {
		CallDataRecord cdr = new CallDataRecord();
		UserDetails subscriberProfile = null; 
		String otherPartyNumber = null;
		
		if(transactionType==2) {
			//MTC-SMS
			subscriberProfile = session.getCdpnProfile();
			otherPartyNumber = session.getOrigCgpn();
				
		} else if(transactionType==1) {
			//MOC-SMS
			subscriberProfile = session.getCgpnProfile();
			otherPartyNumber = session.getOrigCdpn();
		}
		cdr.setTransactionType(transactionType);
		cdr.setMsisdn(reverseEndian(formatSubscriberNumber(subscriberProfile.getPhoneNumber())));
		cdr.setImei(subscriberProfile.getImei()==null?"359707051241823":subscriberProfile.getImei());
		cdr.setImsi(subscriberProfile.getImsi()==null?"214017501576949":subscriberProfile.getImsi());
		//cdr.setImei(subscriberProfile.getImei());
		cdr.setOtherParty(reverseEndian(formatOtherPartyNumber(otherPartyNumber)));
		cdr.setCellId(SSIDMap.getInstance().getCellId(subscriberProfile.getSsid()==null?"":subscriberProfile.getSsid()));
		
		if(session.getCallAnsweredTime()!=null) {
			cdr.setTransactionDate(getFormattedDate(session.getCallAnsweredTime()));
			cdr.setTransactionTime(getFormattedTime(session.getCallAnsweredTime()));
		} else if(session.getCallAttemptedTime()!=null) {
			cdr.setTransactionDate(getFormattedDate(session.getCallAttemptedTime()));
			cdr.setTransactionTime(getFormattedTime(session.getCallAttemptedTime()));
		}
		
		cdr.setChargingDuration("0");
		
		if(session.getCallAnsweredTime()!=null
		   && session.getCallHangupTime()!=null) {
			long duration = (session.getCallHangupTime().getTime() - session.getCallAnsweredTime().getTime())/1000;
			cdr.setChargingDuration(reverse(""+duration));
		}
		return cdr;
	}
	
	private CallDataRecord forSms(SmsSession session) {
		CallDataRecord cdr = new CallDataRecord();
		UserDetails subscriberProfile = null; 
		String otherPartyNumber = null;
		
		cdr.setTransactionDate(getFormattedDate(session.getSqlTimestamp()));
		cdr.setTransactionTime(getFormattedTime(session.getSqlTimestamp()));
		
		if(transactionType==11) {
		//MTC-SMS
			subscriberProfile = session.getCdpnProfile();
			otherPartyNumber = session.getOrigCgpn();
			cdr.setTransactionType(0x1f);
			
		} else if(transactionType==10) {
		//MOC-SMS
			subscriberProfile = session.getCgpnProfile();
			otherPartyNumber = session.getOrigCdpn();
			cdr.setTransactionType(0x1e);
		}
		
		cdr.setMsisdn(formatSubscriberNumber(subscriberProfile.getPhoneNumber()));
		cdr.setImsi(subscriberProfile.getImsi());
		cdr.setImei(subscriberProfile.getImei());
		cdr.setOtherParty(formatOtherPartyNumber(otherPartyNumber));
		//cdr.setCellId(SSIDMap.getInstance().getCellId(subscriberProfile.getSsid()));
		
		return cdr;
	}
	
	private String getFormattedDate(Date timestamp) {
		String ret = reverse("" + (timestamp.getYear()-100));
		int month = timestamp.getMonth()+1;
		int date = timestamp.getDate();
		ret  += reverse((month>9)?(""+month):("0"+month));
		ret  += reverse((date>9)?(""+date):("0"+date));
		return ret;
	}
	
	private String getFormattedTime(Date timestamp) {
		String ret = "";
		int hrs = timestamp.getHours();
		ret += reverse((hrs>9)?(""+hrs):("0"+hrs));
		int mins = timestamp.getMinutes();
		ret += reverse((mins>9)?(""+mins):("0"+mins));
		int secs = timestamp.getSeconds();
		ret += reverse((secs>9)?(""+secs):("0"+secs));
		return ret;
	}
	
	
	private String getFormattedDate(Timestamp sqlTimestamp) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unused")
		String ret = reverse("" + (sqlTimestamp.getYear()-100));
		int month = sqlTimestamp.getMonth()+1;
		int date = sqlTimestamp.getDate();
		ret  += reverse((month>9)?(""+month):("0"+month));
		ret  += reverse((date>9)?(""+date):("0"+date));
		return ret;
	}

	@SuppressWarnings({ "deprecation", "unused" })
	private String getFormattedTime(Timestamp sqlTimestamp) {
		// TODO Auto-generated method stub
		String ret = "";
		int hrs = sqlTimestamp.getHours();
		ret += reverse((hrs>9)?(""+hrs):("0"+hrs));
		int mins = sqlTimestamp.getMinutes();
		ret += reverse((mins>9)?(""+mins):("0"+mins));
		int secs = sqlTimestamp.getSeconds();
		ret += reverse((secs>9)?(""+secs):("0"+secs));
		return ret;
	}

	private String formatSubscriberNumber(String number) {
		int remaining = this.subscriberNumberLength - number.length();
		for(;remaining>0;remaining--) number+="F";
		return number;
	}
	
	private String formatOtherPartyNumber(String number) {
		int remaining = this.otherPartyNumberLength - number.length();
		for(;remaining>0;remaining--) number+="F";
		return number;
	}
	
	private String reverse(String str) {
		return str;
		//return new StringBuffer(str).reverse().toString();
	}
	
	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public String getBasicService() {
		return basicService;
	}

	public void setBasicService(String basicService) {
		this.basicService = basicService;
	}

	public int getSubscriberNumberLength() {
		return subscriberNumberLength;
	}

	public void setSubscriberNumberLength(int subscriberNumberLength) {
		this.subscriberNumberLength = subscriberNumberLength;
	}

	public int getOtherPartyNumberLength() {
		return otherPartyNumberLength;
	}

	public void setOtherPartyNumberLength(int otherPartyNumberLength) {
		this.otherPartyNumberLength = otherPartyNumberLength;
	}

	public String reverseEndian(String in) {
		return in;
		/*
		String out=null;
		char[] inArray=in.toCharArray();
		char[] outArray=new char[inArray.length];
		
		for(int i=0;i<inArray.length;i+=2) {
			outArray[i] = inArray[i+1];
			outArray[i+1] = inArray[i];
		}
		
		out = String.valueOf(outArray, 0, inArray.length);
		return out;*/
	}
}
