package com.movirtu.mxcloudservice.service;

public interface SessionFactory {
	BaseSession createSession();
}
