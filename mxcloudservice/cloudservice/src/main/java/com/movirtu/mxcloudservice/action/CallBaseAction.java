package com.movirtu.mxcloudservice.action;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.BaseSession;
import com.movirtu.mxcloudservice.service.Session;

public abstract class CallBaseAction extends BaseAction{

	public abstract boolean perform(Session session) throws Exception, RestException;
	
	public boolean perform(BaseSession session) throws Exception, RestException {
		// TODO Auto-generated method stub
		return perform((Session) session);
	}

}
