package com.movirtu.mxcloudservice.action.diameter;

import ch.loway.oss.ari4java.tools.RestException;

import com.movirtu.mxcloudservice.service.DiameterSession;
import com.movirtu.mxcloudservice.service.machine.MachineEvent;

public class PushEventOnServiceType extends BaseDiameterAction{

	private char serviceType1;
	private MachineEvent event1;
	
	private char serviceType2;
	private MachineEvent event2;
	
	@Override
	public boolean perform(DiameterSession session) throws Exception,
			RestException {
		// TODO Auto-generated method stub
		
		if(session.getProfile()!=null) {
			if(serviceType1==session.getProfile().getType())
				session.getService().execute(session, event1);
			else if(serviceType2==session.getProfile().getType())
				session.getService().execute(session, event2);
			else
				session.getService().execute(session, MachineEvent.ERROR);			
		} else {
			session.getService().execute(session, MachineEvent.ERROR);
		}
		return true;
	}

	public char getServiceType1() {
		return serviceType1;
	}

	public void setServiceType1(char serviceType1) {
		this.serviceType1 = serviceType1;
	}

	public MachineEvent getEvent1() {
		return event1;
	}

	public void setEvent1(MachineEvent event1) {
		this.event1 = event1;
	}

	public char getServiceType2() {
		return serviceType2;
	}

	public void setServiceType2(char serviceType2) {
		this.serviceType2 = serviceType2;
	}

	public MachineEvent getEvent2() {
		return event2;
	}

	public void setEvent2(MachineEvent event2) {
		this.event2 = event2;
	}

}
