package com.movirtu.mxcloudservice.connector.diameter.rf.cdf;

import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwACRClient;
import com.movirtu.diameter.charging.apps.B2BGw.client.B2BGwSendFromServiceInvocationWrapperForRequest;
import com.movirtu.diameter.charging.apps.common.TransactionInfo;
import com.movirtu.diameter.test.b2bgw.ServiceProcessingDiameterMessages;
import com.movirtu.mxcloudservice.connector.GenericConsumer;
import com.movirtu.mxcloudservice.connector.diameter.rf.OfflineChargingConnector;
import com.movirtu.mxcloudservice.connector.event.ACADiameterEvent;
import com.movirtu.mxcloudservice.connector.event.ACRDiameterEvent;

public class CDFConnector extends OfflineChargingConnector implements ServiceProcessingDiameterMessages{

	B2BGwACRClient server;
	B2BGwSendFromServiceInvocationWrapperForRequest client;

	protected void createClient() {
		// TODO Auto-generated method stub
		client = B2BGwSendFromServiceInvocationWrapperForRequest.INSTANCE;
		actionConsumer = new GenericConsumer(outQueue, this, getActionExecutor(),new CDFConnectorActionConsumer(client));
	}


	protected void createServer() throws Exception {
		// TODO Auto-generated method stub
		eventConsumer = new GenericConsumer(inQueue, this, getEventExecutor() , new CDFConnectorEventConsumer());
		server = new B2BGwACRClient(/*this*/);
		server.init(getClass().getResourceAsStream(getConfigFileName()),"SERVER1");
		server.start();
	}


	public void serviceProcessingACA(TransactionInfo obj) {
		// TODO Auto-generated method stub
		super.getEventQueue().add(new ACADiameterEvent(obj));
	}


	public void serviceProcessingACR(TransactionInfo obj) {
		// TODO Auto-generated method stub
		super.getEventQueue().add(new ACRDiameterEvent(obj));
	}


	public void replyOfACRWrapperToService(TransactionInfo obj) {
		// TODO Auto-generated method stub
		
	}

	
}
