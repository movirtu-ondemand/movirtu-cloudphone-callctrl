create table data_user_profile(id long, active boolean, apn varchar(50), virtual_subscription_id varchar(50),physical_subscription_id varchar(50), virtual_imsi varchar(50), physical_imsi varchar(50), type char, last_changed date);
alter table data_user_profile alter last_changed set default now();
CREATE OR REPLACE FUNCTION upd_timestamp() RETURNS TRIGGER 
LANGUAGE plpgsql
AS
$$
BEGIN
    NEW.last_changed = CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$$;

CREATE TRIGGER t_name
  BEFORE UPDATE
  ON data_user_profile
  FOR EACH ROW
  EXECUTE PROCEDURE upd_timestamp();

create index concurrently apn_index on data_user_profile(physical_subscription_id,apn);
create index concurrently active_index on data_user_profile(physical_subscription_id,active);
create index concurrently apn_imsi_index on data_user_profile(physical_imsi, apn);
create index concurrently active_imsi_index on data_user_profile(physical_imsi,active);
create index concurrently default_subscription_index on data_user_profile(physical_subscription_id);
create index concurrently default_imsi_index on data_user_profile(physical_imsi);
